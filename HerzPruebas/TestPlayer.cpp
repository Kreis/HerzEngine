#include "TestPlayer.h"

TestPlayer::TestPlayer() {
	hrigidbody = std::make_shared<HRigidBody>(HintRect(0, 0, 64, 64));
	HCore::s_hphysics()->add_hrigidbody(hrigidbody);

	himage = std::make_shared<HImage>("data/unit.png", HintRect(64, 128, 64, 64));


	hrigidbody->set_absolute_position(HncVector(200, 0));

	jump_parabola = std::unique_ptr<HParabola>(new HParabola);

	c = 0;
}

void TestPlayer::fixed_update(const ndec& delta_time) {

	hrigidbody->set_move_y(delta_time * 100);

	if (HCore::s_hinput()->is_pressed(HKey::LEFT)) {
		hrigidbody->set_move_x(delta_time * (-600));
	}
	if (HCore::s_hinput()->is_pressed(HKey::RIGHT)) {
		hrigidbody->set_move_x(delta_time * 600);
	}

	if (HCore::s_hinput()->is_typed(HKey::UP)) {
		jump_parabola->initialize(hrigidbody, 0.5, 240, 0, HTypeParabola::VERTICAL_INVERTED);
	}

	if ( ! jump_parabola->update(delta_time) && ! hrigidbody->colliding_down) {
		jump_parabola->initialize(hrigidbody, 0.5, 240, 0.5, HTypeParabola::VERTICAL_INVERTED);
	}
}

void TestPlayer::update(const ndec& delta_time) {

	himage->set_position(hrigidbody->get_position().geti());
	bounds.x() = hrigidbody->get_position().x;
	bounds.y() = hrigidbody->get_position().y;
	static bool f_h = true;
	if (HCore::s_hinput()->is_typed(HKey::DOWN)) {
		himage->set_flip_horizontal(f_h);
		f_h = ! f_h;
	}
}

void TestPlayer::draw(std::shared_ptr<HImageSpace> himagespace) {
	himagespace->draw_himage(himage);
}