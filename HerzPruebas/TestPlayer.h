#ifndef _TESTPLAYER_H_
#define _TESTPLAYER_H_

#include <memory>
#include <iostream>
#include <Herz.h>

class TestPlayer : public HActor {
public:
	TestPlayer();

	void update(const ndec& delta_time) override;
	void fixed_update(const ndec& delta_time) override;
	void draw(std::shared_ptr<HImageSpace> himagespace) override;

private:
	std::shared_ptr<HRigidBody> hrigidbody;
	std::shared_ptr<HImage> himage;

	std::unique_ptr<HParabola> jump_parabola;

	int c;
};

#endif