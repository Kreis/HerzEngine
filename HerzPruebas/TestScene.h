#ifndef _TESTSCENE_H_
#define _TESTSCENE_H_

#include <Herz.h>
#include "TestPlayer.h"
#include "TestScene2.h"

class TestScene : public HScene {
public:
	TestScene();

	void load() override;

	void update(const ndec& delta_time) override;
	void fixed_update(const ndec& delta_time) override;
	void draw(std::shared_ptr<HImageSpace> himagespace) override;

private:
	ndec current_time;

	std::shared_ptr<HActor> player;
	std::shared_ptr<HMap> map;
	std::shared_ptr<HMap> map2;
	std::shared_ptr<HCamera> hcamera;

	std::shared_ptr<HDrawRigidBodies> draw_rigids;

	std::shared_ptr<HImage> himagerender;
	std::shared_ptr<HImage> himagerender_2;

	std::shared_ptr<HTextDialog> htext_dialog;


	std::string the_text;
};

#endif