#include <iostream>
#include <Herz.h>
#include "TestEmptyScene.h"

int main(int argc, char** argv) {
	
    std::shared_ptr<Herz> herz = std::make_shared<Herz>(1000, 800, "Herz");

    std::shared_ptr<HScene> hscene = std::make_shared<TestEmptyScene>();
    herz->start(hscene);

	return 0;
}