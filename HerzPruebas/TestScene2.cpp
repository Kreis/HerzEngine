#include "TestScene2.h"

TestScene2::TestScene2() {

}

void TestScene2::load() {

	vector_texture_priority.push_back("textures/texture_01.png");

	hcamera = herz_interface->get_hcamera();

	himage = std::make_shared<HImage>("textures/texture_01.png", HintRect(0, 0, 100, 64));
}

void TestScene2::fixed_update(const ndec& delta_time) {
	
	for (std::shared_ptr<HActor>& hactor : vector_hactors) {
		hactor->fixed_update(delta_time);
	}
	HCore::s_hphysics()->update();
}

void TestScene2::update(const ndec& delta_time) {

	if (HCore::s_hinput()->is_pressed(HKey::DOWN)) {
		//hcamera->move(HncVector(-20, 0));
	}
	if (HCore::s_hinput()->is_pressed(HKey::RIGHT)) {
		//hcamera->move(HncVector(20, 0));
	}

	for (std::shared_ptr<HActor>& hactor : vector_hactors) {
		hactor->update(delta_time);
	}
	
}

void TestScene2::draw(std::shared_ptr<HImageSpace> himagespace) {

	himagespace->draw_himage(himage);

	for (std::shared_ptr<HActor>& hactor : vector_hactors) {

		hactor->draw(himagespace);
	}
	//draw_rigids->draw(himagespace, "texture/map01.png");

}

