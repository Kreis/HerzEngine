#ifndef _TESTEMPTYSCENE_H_
#define _TESTEMPTYSCENE_H_

#include <Herz.h>
#include <iostream>
#include <memory>

class TestEmptyScene : public HScene {
public:
	TestEmptyScene();

	void fixed_update(const ndec& delta_time) override;
	void update(const ndec& delta_time) override;
	void draw(std::shared_ptr<HImageSpace> himagespace) override;

private:
	void load() override;
};

#endif