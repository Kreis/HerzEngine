#include "TestScene.h"

TestScene::TestScene() {

}

void TestScene::load() {
	map = std::make_shared<HMap>();
	map->load("data/mapa_01.tmx");

	vector_texture_priority.push_back("data/tiles.png");
	vector_texture_priority.push_back("data/unit.png");

	hcamera = herz_interface->get_hcamera();

	HCore::s_hphysics();
	HCore::s_hphysics()->add_rigid_map("data/mapa_01.tmx");

	player = std::make_shared<TestPlayer>();
	add_hactor(player);

	// himagerender = std::make_shared<HImage>("textures/texture_01.png", HintRect(0, 0, 100, 64));

	// htext_dialog = std::make_shared<HTextDialog>("data/fonts/meow.fnt", HintRect(200, 200, 300, 150), 0.05);

	// add_hactor(htext_dialog);

	// HCore::s_hlscript()->export_var("the_text", this->the_text);

	//std::shared_ptr<HSlope> hslope = std::make_shared<HSlope>(HncVector(800, 64*8), HncVector(700, 64*9));
	//HCore::s_hphysics()->add_hslope(hslope);

	//draw_rigids = std::make_shared<HDrawRigidBodies>();
}

void TestScene::fixed_update(const ndec& delta_time) {
	
	for (std::shared_ptr<HActor>& hactor : vector_hactors) {
		hactor->fixed_update(delta_time);
	}
	HCore::s_hphysics()->update();
}

void TestScene::update(const ndec& delta_time) {

	if (HCore::s_hinput()->is_pressed(HKey::DOWN)) {
		//hcamera->move(HncVector(-20, 0));
	}
	if (HCore::s_hinput()->is_pressed(HKey::RIGHT)) {
		//hcamera->move(HncVector(20, 0));
	}

	for (std::shared_ptr<HActor>& hactor : vector_hactors) {
		hactor->update(delta_time);
	}
	
}

void TestScene::draw(std::shared_ptr<HImageSpace> himagespace) {

	static HncVector position_to_camera;
	position_to_camera.x = player->get_bounds().get_x();
	position_to_camera.y = player->get_bounds().get_y();

	static bool _x = false;
/*
	if (HCore::s_hinput()->is_typed(HKey::UP)) {
		HCore::s_hlscript()->import_data("data/vars.txt");
		htext_dialog->set_text(the_text);
		finish();
	}
*/
	if (HCore::s_hinput()->is_typed(HKey::LEFT)) {
		//htext_dialog->next();
	}

	if (HCore::s_hinput()->is_typed(HKey::DOWN)) {
		// _x = !_x;
		//htext_dialog->set_text("etwas anderes");
	}
	/*
	if (_x) {
		himagespace->draw_himage(himagerender);
		himagespace->draw(hrendertexture);

		static bool akk = true;
		if (akk) {

			std::shared_ptr<HTexture> htexture_rt = HCore::s_hfactory()->make_htexture(hrendertexture);

			himagerender_2 = std::make_shared<HImage>(htexture_rt, "texture_rt", HintRect(0,0,60,60));
			akk = false;
		}
		
		himagespace->draw_himage(himagerender_2);

		hcamera->set_center_x(position_to_camera.x);

		herz_interface->load_hscene(std::make_shared<TestScene2>());
	} else {
		hcamera->set_center(position_to_camera);
	}
	*/
	map->draw(himagespace, hcamera->get_view_rect());
	//map2->draw(himagespace, hcamera->get_view_rect());

	for (std::shared_ptr<HActor>& hactor : vector_hactors) {

		hactor->draw(himagespace);
	}
	//draw_rigids->draw(himagespace, "texture/map01.png");

}

