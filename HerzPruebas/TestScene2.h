#ifndef _TESTSCENE2_H_
#define _TESTSCENE2_H_

#include <Herz.h>

class TestScene2 : public HScene {
public:
	TestScene2();

	void load() override;

	void update(const ndec& delta_time) override;
	void fixed_update(const ndec& delta_time) override;
	void draw(std::shared_ptr<HImageSpace> himagespace) override;

private:
	std::shared_ptr<HCamera> hcamera;

	std::shared_ptr<HImage> himage;
};

#endif