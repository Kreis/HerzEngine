#include "TestEmptyScene.h"

TestEmptyScene::TestEmptyScene() {

}

void TestEmptyScene::fixed_update(const ndec& delta_time) {

}

void TestEmptyScene::update(const ndec& delta_time) {


	if (HCore::s_hinput()->is_typed(HMouseButton::LEFT)) {
		std::cout << "typed!" << std::endl;
		HintVector pos = HCore::s_hinput()->get_mouse_position(true);
		std::cout << pos.x << " " << pos.y << std::endl;
		pos.x = 100;
		pos.y = 100;
		HCore::s_hinput()->set_mouse_position(pos, true);
	}

	std::cout << "wheel:" << HCore::s_hinput()->get_mouse_wheel_delta() << std::endl;
	// std::cout << "wheel: " <<  << std::endl;
	// if (hmouse->is_released(MouseButton::LEFT)) {
	// 	std::cout << "released!" << std::endl;
	// 	hmouse->set_position(100, 100)
	// }

}

void TestEmptyScene::draw(std::shared_ptr<HImageSpace> himagespace) {

}

void TestEmptyScene::load() {

}