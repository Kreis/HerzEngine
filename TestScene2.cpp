#include "TestScene2.h"
#include "TestScene.h"

TestScene2::TestScene2() {

}

void TestScene2::load() {

}

void TestScene2::update(const ndec& delta_time, std::shared_ptr<HImageSpace> himagespace) {


	current_time += delta_time;

	if (current_time >= 2) {
		std::cout << " scene 2!! " << std::endl;
		current_time = 0;
	}

	if (HCore::s_hinput()->is_typed(HKey::LEFT)) {
		std::cout << "typed left" << std::endl;

		auto new_scene = std::make_shared<TestScene>();
		herz_interface->load_hscene(new_scene);
	}

}