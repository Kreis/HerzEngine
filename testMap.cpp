#include <Herz.h>
#include <iostream>
#include <memory>
#include <vector>

int main(int argc, char** argv) {

    HWindow hwindow(600, 400, "Herz", 60);
    std::shared_ptr<HDrawFPS> drawFps = HCore::s_hfactory()->make_hdrawfps("arial.ttf");

    std::shared_ptr<HImageSpace> himagespace = std::make_shared<HImageSpace>();
    std::vector<std::string> prio;
    prio.push_back("texture/map01.png");
    prio.push_back("texture/map02.png");
    prio.push_back("texture/map03.png");
    prio.push_back("texture/img2.png");
    
    himagespace->set_texture_priority(prio);

    std::vector<std::shared_ptr<HImage> > vec_himages;

    for (int i = 0; i < 5; i++) {
    	for (int j = 0; j < 5; j++) {
    		std::shared_ptr<HImage> himage = std::make_shared<HImage>("texture/img2.png", HintRect(0,0,50,50));
    		HintVector pos(i * 30, j * 30);
    		himage->set_position(pos);
            himage->set_blend_type(HBlend::BLEND_ADD);
    		vec_himages.push_back(himage);
    	}
    }

    HMap map;
    map.load("veamos");
    std::shared_ptr<HTimer> timer = HCore::s_hfactory()->make_htimer();

    while (true) {
        hwindow.update_input();
        if (HCore::s_hinput()->closed) {
            break;
        }

        hwindow.clear();
        for (std::shared_ptr<HImage> v : vec_himages) {
        	himagespace->draw_himage(v);
        }

        //map.draw(himagespace);

        hwindow.draw(himagespace);

        hwindow.draw(drawFps, timer->get_delta_time());
        hwindow.display();

    }

	return 0;
}
