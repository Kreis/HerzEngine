
#include <memory>
#include <iostream>
#include <Herz.h>

int main(int argc, char** argv) {

	std::shared_ptr<HPhysics> hpysics = std::make_shared<HPhysics>();

	std::shared_ptr<HWindow> hwindow = std::make_shared<HWindow>(600, 500, "test Physics");

	std::shared_ptr<HImageSpace> himagespace = std::make_shared<HImageSpace>();

	std::vector<std::string> tex_priority;
	tex_priority.push_back("texture/img2.png");
	himagespace->set_texture_priority(tex_priority);

	std::shared_ptr<HRigidBody> objekt = std::make_shared<HRigidBody>(HintRect(0,0,120,40));
	objekt->dynamic = true;
	objekt->set_position_y(200);
	hpysics->push_hrigidbody(objekt);

	std::shared_ptr<HRigidBody> boden = std::make_shared<HRigidBody>(HintRect(0,0,120,40));
	boden->dynamic = false;
	boden->set_position(HncVector(0, 300));
	hpysics->push_hrigidbody(boden);

	std::shared_ptr<HRigidBody> mauer = std::make_shared<HRigidBody>(HintRect(0,0,120,40));
	mauer->dynamic = false;
	mauer->set_position(HncVector(200, 280));
	hpysics->push_hrigidbody(mauer);

	HDrawRigidBodies test_draw;

	std::shared_ptr<HTimer> htimer = HCore::s_hfactory()->make_htimer();

	bool flag = true;

	HParabola par;

	while (hwindow->is_open()) {

		const ndec t = htimer->get_delta_time();
		hwindow->update_input();

		hwindow->clear();

		if (HCore::s_hinput()->is_typed(HKey::UP) && flag) {
			par.initialize(objekt, 1, 100, 0, HTypeParabola::HORIZONTAL);
			flag = false;
		}
		if ( ! par.update(t) && ! flag) {
			std::cout << "auf boden!" << std::endl;
			flag = true;
		}

		if (HCore::s_hinput()->is_pressed(HKey::RIGHT)) {
			objekt->add_move_x(20);
		}
		if (HCore::s_hinput()->is_pressed(HKey::LEFT)) {
			objekt->add_move_x(-20);
		}


		hpysics->fixed_update(0.17);
		test_draw.draw(hpysics, himagespace, "texture/img2.png");

		hwindow->draw(himagespace);
		hwindow->display();

	}

	return 0;
}