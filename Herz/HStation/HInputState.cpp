#include "HInputState.h"
#include <HCore/HCore.h>

HInputState::HInputState() {
	hevent = HCore::s_hfactory()->make_hevent();
	hmouse = HCore::s_hfactory()->make_hmouse();
	closed = false;
	shift = false;
	alt = false;
	control = false;
	resized = false;
	mouse_wheel_delta = 0;
	focus_here = true;
	mouse_moving = false;
	mouse_here = true;
}

void HInputState::update_state(std::shared_ptr<HRenderWindow> hrenderwindow) {
	this->hrenderwindow = hrenderwindow;
	hmouse->update_mouse_state();

	vector_released.clear();
	shift 	= false;
	alt 	= false;
	control = false;
	resized = false;
	mouse_moving = false;

	update_typed_to_pressed(buffer_input);

	while (hrenderwindow->poll_event(hevent)) switch (hevent->get_type()) {
		case TypeEvent::CLOSED:
			closed = true;
			break;
		// this pressed works with delay after first typed
		case TypeEvent::KEY_PRESSED:
			key_pressed_event();
			break;
		case TypeEvent::KEY_RELEASED:
			key_released_event();
			break;
		case TypeEvent::RESIZED:
			resized = true;
			break;
		case TypeEvent::LOST_FOCUS:
			focus_here = false;
			break;
		case TypeEvent::GAINED_FOCUS:
			focus_here = true;
			break;
		case TypeEvent::MOUSE_WHEEL_MOVED:
			int x;
			mouse_wheel_delta = 0;
			hevent->get_mouse_wheel_delta(x, mouse_wheel_delta);
			break;
		case TypeEvent::MOUSE_MOVED:
			mouse_moving = true;
			break;
		case TypeEvent::MOUSE_ENTERED:
			mouse_here = true;
			break;
		case TypeEvent::MOUSE_LEFT:
			mouse_here = false;
			break;
		case TypeEvent::EMPTY:
			break;
		default:
			break;
	}
}

void HInputState::set_mouse_position(HintVector& position, bool relative) {
	if (relative) {
		hmouse->set_position(position, hrenderwindow);
		return;
	}

	hmouse->set_position(position);
}

void HInputState::set_mouse_position(int x, int y, bool relative) {
	if (relative) {
		hmouse->set_position(x, y, hrenderwindow);
		return;
	}
	
	hmouse->set_position(x, y);
}

HintVector HInputState::get_mouse_position(bool relative) {
	if (relative)
		return hmouse->get_position(hrenderwindow);

	return hmouse->get_position();	
}

int HInputState::get_mouse_wheel_delta() {
	return mouse_wheel_delta;
}

bool HInputState::is_mouse_moving() {
	return mouse_moving;
}

bool HInputState::is_focus_here() {
	return focus_here;
}

bool HInputState::is_mouse_here() {
	return mouse_here;
}

bool HInputState::is_pressed(HKey key) {
	return buffer_input[ key ] > 0;
}

bool HInputState::is_typed(HKey key) {
	return buffer_input[ key ] == 1;
}

bool HInputState::is_released(HKey key) {
	for (int v : vector_released) {
		if (v == key)
			return true;
	}

	return false;
}

bool HInputState::is_pressed(HMouseButton button) {
	return hmouse->is_pressed(button);
}

bool HInputState::is_typed(HMouseButton button) {
	return hmouse->is_typed(button);
}

bool HInputState::is_released(HMouseButton button) {
	return hmouse->is_released(button);
}

std::shared_ptr<HEvent> HInputState::get_hevent() {
	return hevent;
}

bool HInputState::is_shift() {
	return shift;
}
bool HInputState::is_alt() {
	return alt;
}
bool HInputState::is_control() {
	return control;
}
bool HInputState::is_resized() {
	return resized;
}

void HInputState::reset_state() {
	vector_released.clear();
	buffer_input.fill(0);
}

// private 
void HInputState::update_typed_to_pressed(std::array<int, HKey::COUNT>& buffer_input) {

	for (int i = 0; i < HKey::COUNT; i++) if (buffer_input[ i ] == 1) {
		buffer_input[ i ] = 2;
	}

}

void HInputState::key_pressed_event() {

	key_code = hevent->get_key_code();
	buffer_value = buffer_input[ key_code ];

	buffer_input[ key_code ] = buffer_value == 0 ? 1 : buffer_value;

	shift 	= hevent->is_shift();
	alt		= hevent->is_alt();
	control	= hevent->is_control();

}

void HInputState::key_released_event() {

	key_code = hevent->get_key_code();
	vector_released.push_back(key_code);

	buffer_input[ key_code ] = 0;

}



