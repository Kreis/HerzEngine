#include "HWindow.h"

HWindow::HWindow(int width, int height, std::string title, int framerate, bool fullscreen) {
	hrenderwindow = HCore::s_hfactory()->make_hrender_window(width, height, title, framerate, fullscreen);
	this->fullscreen = fullscreen;
	this->title = title;
	this->framerate = framerate;
	initial_width = width;
	initial_height = height;
	
	hcamera = HCore::s_hfactory()->make_hcamera(HintRect(0,0,width,height));
	hrenderwindow->set_camera(hcamera);
}

bool HWindow::is_open() {
	return hrenderwindow->is_open();
}
void HWindow::clear() {
	hrenderwindow->clear();
}
void HWindow::update_input() {
	HCore::s_hinput()->update_state(hrenderwindow);

	if (HCore::s_hinput()->is_resized()) {
		adjust_viewport(hcamera);
		hrenderwindow->set_camera(hcamera);
	}
	if (HCore::s_hinput()->closed) {
		hrenderwindow->close();
	}
	if (hcamera->modified) {
		hrenderwindow->set_camera(hcamera);
	}
}
void HWindow::draw(std::shared_ptr<HImageSpace> himagespace, bool clear_hrender, bool display_hrender) {
	himagespace->draw(hrenderwindow, clear_hrender, display_hrender);
}
void HWindow::draw(std::shared_ptr<HDrawFPS> hdrawfps, const ndec& delta_time) {
	const HintRect hcamera_position = hcamera->get_view_rect();
	hdrawfps->draw_fps(hrenderwindow, delta_time, hcamera_position.get_x(), hcamera_position.get_y());
}
void HWindow::display() {
	hrenderwindow->display();
}

void HWindow::set_fullscreen(bool on) {
	if (on == fullscreen)
		return;
	fullscreen = on;
	int w, h;
	if (on) {
		hrenderwindow->get_size_screen(w, h);
		hrenderwindow->create(w, h, title, framerate, on);
	} else {
		w = initial_width;
		h = initial_height;
		hrenderwindow->create(w, h, title, framerate, on);
	}
	HCore::s_hinput()->reset_state();

	adjust_viewport(hcamera);
	hrenderwindow->set_camera(hcamera);
}

std::shared_ptr<HCamera> HWindow::get_hcamera() {
	return hcamera;
}

// private
void HWindow::adjust_viewport(std::shared_ptr<HCamera> hcamera) {
	ndec width_original = initial_width;
	ndec height_original = initial_height;

	int width_frame_int, height_frame_int;
	hrenderwindow->get_size_frame(width_frame_int, height_frame_int);
	ndec width_frame 	= width_frame_int;
	ndec height_frame 	= height_frame_int;

	ndec ratio_original = width_original / height_original;
	ndec ratio_frame 	= width_frame / height_frame;

	ndec x, y;
	if (ratio_original >= ratio_frame) {
		width_original 	= width_frame;
		height_original = width_original / ratio_original;
		x = 0;
		y = (height_frame - height_original) / 2;

	} else {
		height_original = height_frame;
		width_original 	= height_original * ratio_original;

		x = (width_frame - width_original) / 2;
		y = 0;

	}

	ndec f_x = x / width_frame;
	ndec f_y = y / height_frame;
	ndec f_width = width_original / width_frame;
	ndec f_height = height_original / height_frame;

	hcamera->set_viewport(HncRect(f_x, f_y, f_width, f_height));
}