#ifndef _HWINDOW_H_
#define _HWINDOW_H_

#include <memory>
#include <iostream>
#include <HCore/HCore.h>
#include <HBasic/ndec.h>
#include <HBasic/HintRect.h>
#include <HBasic/HncRect.h>
#include <HRender/HImageSpace.h>

class HWindow {
public:
	HWindow(int width, int height, std::string title, int framerate = 60, bool fullscreen = false);

	bool is_open();
	void update_input();
	void clear();
	void draw(std::shared_ptr<HImageSpace> himagespace, bool clear_hrender = true, bool display_hrender = true);
	void draw(std::shared_ptr<HDrawFPS> hdrawfps, const ndec& delta_time);
	void display();

	std::shared_ptr<HCamera> get_hcamera();

	void set_fullscreen(bool on);

private:
	std::shared_ptr<HRenderWindow> hrenderwindow;
	std::shared_ptr<HCamera> hcamera;
	bool fullscreen;
	int framerate;
	std::string title;
	int initial_width;
	int initial_height;

	void adjust_viewport(std::shared_ptr<HCamera> hcamera);
};


#endif