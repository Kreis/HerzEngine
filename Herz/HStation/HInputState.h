#ifndef _HINPUTSTATE_H_
#define _HINPUTSTATE_H_

#include <memory>
#include <iostream>
#include <array>
#include <vector>
#include <HBasic/HKey.h>
#include <HBasic/HintVector.h>

class HRenderWindow;
class HEvent;
class HMouse;
enum class HMouseButton;
class HInputState {
public:
	HInputState();

	bool closed;
	bool is_pressed(HKey key);
	bool is_typed(HKey key);
	bool is_released(HKey key);
	bool is_pressed(HMouseButton key);
	bool is_typed(HMouseButton key);
	bool is_released(HMouseButton key);
	bool is_shift();
	bool is_alt();
	bool is_control();
	bool is_resized();
	void update_state(std::shared_ptr<HRenderWindow> hrenderwindow);
	void set_mouse_position(HintVector& position, bool relative = true);
	void set_mouse_position(int x, int y, bool relative = true);
	HintVector get_mouse_position(bool relative = true);
	int get_mouse_wheel_delta();
	bool is_mouse_moving();
	bool is_focus_here();
	bool is_mouse_here();
	void reset_state();

	std::shared_ptr<HEvent> get_hevent();

private:
	std::shared_ptr<HRenderWindow> hrenderwindow;
	std::array<int, HKey::COUNT> buffer_input;
	std::vector<int> vector_released;
	std::shared_ptr<HEvent> hevent;
	std::shared_ptr<HMouse> hmouse;

	int key_code;
	int buffer_value;

	bool shift;
	bool alt;
	bool control;
	bool resized;

	int mouse_wheel_delta;
	bool focus_here;
	bool mouse_moving;
	bool mouse_here;

	void update_typed_to_pressed(std::array<int, HKey::COUNT>& buffer_input);
	void key_pressed_event();
	void key_released_event();
};

#endif