#ifndef _HERZ_H_
#define _HERZ_H_

#include <memory>
#include <iostream>
#include <HCore/HCore.h>
#include <HBasic/ndec.h>
#include <HStation/HWindow.h>
#include <HRender/HImageSpace.h>
#include <HEntities/HAnimator.h>
#include <HEntities/HAnimation.h>
#include <HEntities/HScene.h>
#include <HEntities/HerzInterface.h>
#include <HEntities/HMap.h>
#include <HPhysics/HPhysics.h>
#include <HPhysics/HParabola.h>
#include <HRender/HDrawRigidBodies.h>
#include <HRender/HRenderImage.h>
#include <HEntities/HTextDialog.h>

class Herz : public HerzInterface {
public:
	Herz(
		int width,
		int height,
		std::string title,
		int frame_rate = 60,
		bool fullscreen = false);

	void start(std::shared_ptr<HScene> hscene);

	void load_hscene(std::shared_ptr<HScene> hscene) override;
	std::shared_ptr<HCamera> get_hcamera() override;

private:
	ndec fixed_time;
	std::shared_ptr<HScene> current_hscene;
	std::shared_ptr<HScene> new_hscene;
	std::shared_ptr<HWindow> hwindow;
	std::shared_ptr<HImageSpace> himagespace;
	std::shared_ptr<HTimer> htimer;
	std::shared_ptr<HDrawFPS> draw_fps;
	std::shared_ptr<HDrawFPS> get_draw_fps();

	int fullscreen;
	int debug_mode;

	std::vector<std::string>* vector_texture_priority;
};

#endif