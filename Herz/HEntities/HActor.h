#ifndef _HACTOR_H_
#define _HACTOR_H_

#include <memory>
#include <iostream>
#include <HBasic/HncRect.h>
#include <HEntities/HSceneInterface.h>
#include <HRender/HImageSpace.h>

class HActor {
public:
	HActor();

	virtual void set_hscene_interface(std::shared_ptr<HSceneInterface> hsceneinterface);

	const HintRect get_bounds();
	const std::string& get_type();

	virtual void update(const ndec& delta_time) = 0;
	virtual void fixed_update(const ndec& delta_time) = 0;
	virtual void draw(std::shared_ptr<HImageSpace> himagespace) = 0;

protected:
	HncRect bounds;
	std::string type;
	std::shared_ptr<HSceneInterface> get_hsceneinterface();

private:
	std::shared_ptr<HSceneInterface> hsceneinterface;
};

#endif