#ifndef _HANIMATOR_H_
#define _HANIMATOR_H_

#include <memory>
#include <iostream>
#include <HEntities/HAnimation.h>
#include <HRender/HImageSpace.h>
#include <HBasic/HncVector.h>
#include <HBasic/ndec.h>
#include <HResources/HNodeInfo.h>
#include <map>

class HAnimator {
public:
	HAnimator();

	void load(std::shared_ptr<HNodeInfo> hnodeinfo_animator);
	void add_hanimation(std::string name, std::shared_ptr<HAnimation> hanimation);
	void current_hanimation(std::string name);

	void update(const ndec& delta_time);
	void play();
	void pause();
	void stop();

	void draw(std::shared_ptr<HImageSpace> himagespace, const HncVector& pivote_position);

private:
	std::shared_ptr<HAnimation> hanimation;
	std::map<std::string, std::shared_ptr<HAnimation> > map_animations;
};

#endif