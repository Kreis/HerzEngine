#include "HTextDialog.h"

HTextDialog::HTextDialog(const std::string& bitmap_font_path, const HintRect& area, ndec letter_delay, bool relative) {
	this->area = area;
	this->letter_delay = letter_delay;
	this->relative = relative;

	init_bitmap_font(bitmap_font_path);

	waiting = false;
	completed = true;
	active = false;

	active_kerning = true;

	start_index = 0;
	stop_index = 0;

	HCore::s_hlscript()->export_var("delay_letter", this->letter_delay);
}

void HTextDialog::update(const ndec& delta_time) {
	if ( ! active)
		return;

	current_time += delta_time;

	if (current_time < letter_delay) {
		return;
	}

	while (current_time > letter_delay) {
		current_time -= letter_delay;

		if (current_index == text.length() - 1) {
			current_time = 0;
			waiting = false;
			completed = true;
			return;
		}

		if (current_index == stop_index) {
			current_time = 0;
			waiting = true;

			return;
		}

		current_index++;
	}
}

void HTextDialog::fixed_update(const ndec& delta_time) {
	
}

void HTextDialog::draw(std::shared_ptr<HImageSpace> himagespace) {
	if ( ! active)
		return;

	for (int i = start_index; i <= current_index; i++) {
		if (text[ i ] == ' ')
			continue;

		const HLetter& hletter = get_hletter(text[ i ]);

		const std::shared_ptr<HImage>& himage = map_himage[ hletter.id_texture ];

		himage->set_texture_rect(hletter.texture_rect);

		if (relative) {
			const HintRect& view_area = hcamera->get_view_rect();
	
		 	relative_position.x = vector_position_hletter[ i ].x + view_area.get_position().x;
		 	relative_position.y = vector_position_hletter[ i ].y + view_area.get_position().y;
	
			himage->set_position(relative_position);
	
		} else {
			himage->set_position(vector_position_hletter[ i ]);
		}
		
		himage->set_size(hletter.texture_rect.get_size());

		himagespace->draw_himage(himage);
	}
}

void HTextDialog::set_hscene_interface(std::shared_ptr<HSceneInterface> hsceneinterface) {
	HActor::set_hscene_interface(hsceneinterface);

	if (relative)
		hcamera = get_hsceneinterface()->get_hcamera();
}

void HTextDialog::change_font(const std::string& bitmap_font_path) {
	init_bitmap_font(bitmap_font_path);
}

void HTextDialog::next() {
	if (completed) {
		active = false;
		return;
	}

	if ( ! waiting)
		return;

	start_index = stop_index + 2;
	stop_index = queue_stop_index.empty() ? text.length() - 1 : get_stop_index();
	current_index = start_index;

	waiting = false;
}

void HTextDialog::set_text(std::string text) {
	if (text.length() == 0)
		return;

	this->text = text;
	
	cursor.x = 0;
	cursor.y = 0;
	vector_position_hletter.clear();
	queue_stop_index = std::queue<int>();

	start_index = 0;
	stop_index = 0;
	current_index = 0;
	waiting = false;
	completed = false;
	active = true;

	int width_word = 0;
	bool fit_word = false;
	int last_end_index = 0;

	HintVector current_position;
	for (int i = 0; i < text.length(); i++) {
		if (text[ i ] == ' ') {
			width_word = 0;
			fit_word = false;
			cursor.x += space_width;

			vector_position_hletter.push_back(current_position);
			continue;
		}

		if (cursor.y + line_height > area.get_height()) {

			queue_stop_index.push(last_end_index);
			cursor.x = 0;
			cursor.y = 0;
			i--;

			continue;
		}

		if (width_word == 0)
			width_word = get_word_width(text, i);

		if (width_word > area.get_width())
			std::cout << "Error: word too big for the HTextDialog area" << std::endl;

		if ( ! fit_word)
			fit_word = cursor.x + width_word <= area.get_width();

		if ( ! fit_word) {
			cursor.x = 0;
			cursor.y += line_height;

			i--;

			continue;
		}
		
		current_position = cursor;

		const HLetter& hletter = get_hletter(text[ i ]);

		int kerning_distance = 0;
		if (active_kerning && i > 0)
			kerning_distance = get_kerning_distance(text[ i - 1 ], text[ i ]);

		current_position.x += area.get_x() + hletter.xoffset + kerning_distance;
		current_position.y += area.get_y() + hletter.yoffset;

		vector_position_hletter.push_back(current_position);

		cursor.x += hletter.xadvance + kerning_distance;
		last_end_index = i;
	}

	stop_index = queue_stop_index.empty() ? text.length() - 1 : get_stop_index();

	completed = false;
	waiting = false;
}

bool HTextDialog::is_waiting() {
	return waiting;
}

bool HTextDialog::is_completed() {
	return completed;
}

bool HTextDialog::is_kerning() {
	return active_kerning;
}

void HTextDialog::set_kerning(bool v) {
	active_kerning = v;
}

// private
void HTextDialog::init_bitmap_font(const std::string& bitmap_font_path) {
	hnode_info = HCore::s_hresources()->get_hnodeinfo_bitmap_font(bitmap_font_path);

	const std::string& line_height_s = hnode_info->get_attribute("line_height");
	line_height = atoi(line_height_s.c_str());

	const std::string& space_width_s = hnode_info->get_attribute("space_width");
	space_width = atoi(space_width_s.c_str());

	for (const HNodeInfo& page : hnode_info->get_children("pages")[0].get_children("page")) {
		const std::string& id_page = page.get_attribute("id");
		const std::string& texture_path = page.get_attribute("file");
		
		map_himage[ id_page ] = std::make_shared<HImage>(texture_path);
	}

	for (const HNodeInfo& char_n : hnode_info->get_children("chars")[0].get_children("char")) {
		const std::string& ascii = char_n.get_attribute("ascii");
		const std::string& x_s = char_n.get_attribute("x");
		const std::string& y_s = char_n.get_attribute("y");
		const std::string& width_s = char_n.get_attribute("width");
		const std::string& height_s = char_n.get_attribute("height");
		const std::string& xoffset_s = char_n.get_attribute("xoffset");
		const std::string& yoffset_s = char_n.get_attribute("yoffset");
		const std::string& xadvance_s = char_n.get_attribute("xadvance");
		const std::string& id_texture = char_n.get_attribute("page");

		char letter = atoi(ascii.c_str());

		int x = atoi(x_s.c_str());
		int y = atoi(y_s.c_str());
		int width = atoi(width_s.c_str());
		int height = atoi(height_s.c_str());
		int xoffset = atoi(xoffset_s.c_str());
		int yoffset = atoi(yoffset_s.c_str());
		int xadvance = atoi(xadvance_s.c_str());
		
		vector_letter.push_back(HLetter(letter, x, y, width, height, xoffset, yoffset, xadvance, id_texture));
		int id_vector_letter = vector_letter.size();
		map_letter[ letter ] = id_vector_letter;
	}

	for (const HNodeInfo& kerning : hnode_info->get_children("kernings")[0].get_children("kerning")) {
		const std::string& first_s = kerning.get_attribute("first");
		const std::string& second_s = kerning.get_attribute("second");
		const std::string& amount_s = kerning.get_attribute("amount");
		char first = atoi(first_s.c_str());
		char second = atoi(second_s.c_str());
		int amount = atoi(amount_s.c_str());
		auto it = map_kerning.find( first );
		if (it == map_kerning.end()) {
			map_kerning[ first ] = std::map<char, int>();
		}
		
		map_kerning[ first ][ second ] = amount;
	}
}

int HTextDialog::get_kerning_distance(const char& A, const char& B) {
	auto it = map_kerning.find( A );
	if (it == map_kerning.end())
		return 0;

	return map_kerning[ A ][ B ];
}

int HTextDialog::get_word_width(const std::string& text, int id) {
	int width = 0;
	int kerning_distance;
	for (int i = id; i < text.length(); i++) {
		if (text[ i ] == ' ')
			return width;

		kerning_distance = 0;
		if (active_kerning && i > 0)
			kerning_distance = get_kerning_distance(text[ i - 1 ], text[ i ]);

		width += get_hletter(text[ i ]).xadvance + kerning_distance;
	}

	return width;
}

const HLetter& HTextDialog::get_hletter(char hletter) {
	int v = map_letter[ hletter ];

	if (v <= 0)
		std::cout << "Error: map_hletter doesn't have " << hletter << " character" << std::endl;

	if (v > vector_letter.size())
		std::cout << "Error: vector_letter overflow with " << v << " index" << std::endl;

	return vector_letter.at(v - 1);
}

int HTextDialog::get_stop_index() {
	if (queue_stop_index.empty())
		std::cout << "Error: queue_stop_index is empty" << std::endl;

	int t = queue_stop_index.front();
	queue_stop_index.pop();
	return t;
}
