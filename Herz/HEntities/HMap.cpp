#include "HMap.h"

HMap::HMap() {
	
}

void HMap::load(std::string path, HintVector position) {

	vector_himages.clear();
	vector_layers.clear();

	std::shared_ptr<HNodeInfo> hnode_map = HCore::s_hresources()->get_hnodeinfo_map(path);

	global_tile_size.x 	= atoi(hnode_map->get_attribute("tilewidth").c_str());
	global_tile_size.y 	= atoi(hnode_map->get_attribute("tileheight").c_str());
	global_grid_size.x	= atoi(hnode_map->get_attribute("width").c_str());
	global_grid_size.y	= atoi(hnode_map->get_attribute("height").c_str());

	const std::vector<HNodeInfo>& vector_tileset = hnode_map->get_children("tileset");

	for (const HNodeInfo& tileset : vector_tileset) {
		int tilewidth 	= atoi(tileset.get_attribute("tilewidth").c_str());
		int tileheight 	= atoi(tileset.get_attribute("tileheight").c_str());

		int tilecount 	= atoi(tileset.get_attribute("tilecount").c_str());
		int tilecolumns = atoi(tileset.get_attribute("columns").c_str());

		std::string image_source = tileset.get_attribute("image_source");
		int image_width 	= atoi(tileset.get_attribute("image_width").c_str());
		int image_height 	= atoi(tileset.get_attribute("image_height").c_str());


		int i = 0, j = 0;
		for (int count = 0; count < tilecount; count++) {

			int x = i * tilewidth;
			int y = j * tileheight;

			HintRect texture_rect(x, y, tilewidth, tileheight);
			std::shared_ptr<HImage> himage = std::make_shared<HImage>(image_source, texture_rect);
			vector_himages.push_back(himage);

			i++;

			if (i == tilecolumns) {
				i = 0;
				j++;
			}
		}
	}


	const std::vector<HNodeInfo>& vector_layer = hnode_map->get_children("layer");


	int num_layer = 0;
	for (const HNodeInfo& layer : vector_layer) {
		std::string name = layer.get_attribute("name");
		std::string visible = layer.get_attribute("visible");

		if (name == "rigid" || visible == "0")
			continue;

		int layer_width = atoi(layer.get_attribute("width").c_str());
		int layer_height = atoi(layer.get_attribute("height").c_str());

		std::string grid_string = layer.get_value();

		std::unique_ptr<std::vector<int> > layer_grid = std::unique_ptr<std::vector<int> >(new std::vector<int>());
		HCore::s_hresources()->csv_to_vector(grid_string, *layer_grid.get());
		
		int id_vector_layer = vector_layers.size();
		
		vector_layers.push_back(HMapLayer(position, global_grid_size, global_tile_size));
		vector_layers[ id_vector_layer ].set_grid(std::move(layer_grid));

		num_layer++;
	}

}

void HMap::draw(std::shared_ptr<HImageSpace> himagespace, const HintRect& view_area) {
	
	for (HMapLayer& hmaplayer : vector_layers) {
		hmaplayer.draw(himagespace, view_area, vector_himages);
	}
	
}
