#ifndef _HLETTER_H_
#define _HLETTER_H_

#include <iostream>
#include <memory>
#include <HBasic/HintRect.h>

class HLetter {
public:
	HLetter(char letter, int x, int y, int width, int height, int xoffset, int yoffset, int xadvance, std::string id_texture);

	char letter;
	HintRect texture_rect;
	int xoffset;
	int yoffset;
	int xadvance;
	std::string id_texture;
};

#endif