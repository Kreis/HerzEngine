#ifndef _HANIMATION_H_
#define _HANIMATION_H_

#include <memory>
#include <iostream>
#include <HRender/HImage.h>
#include <HBasic/ndec.h>
#include <HBasic/HintRect.h>
#include <HCore/HCore.h>
#include <HRender/HImageSpace.h>
#include <HResources/HNodeInfo.h>
#include <stdlib.h>
#include <vector>

enum class HAnimationStatus {
	PLAY,
	PAUSE,
	STOP
};

class HAnimation {
public:
	HAnimation();

	void load(std::shared_ptr<HNodeInfo> hnodeinfo_animation);
	void load(const HNodeInfo& hnodeinfo_animation);
	void set_texture(const std::string& texture_path);
	void add_frame(const HintRect& texture_rect, const ndec& frame_time, HintVector frame_position = 0);
	void update(const ndec& delta_time);
	void play();
	void pause();
	void stop();
	void set_position(const HintVector& position);

	void draw(std::shared_ptr<HImageSpace> himagespace, const HintVector& pivote_position);

	bool bucle;

private:
	int fps;
	int loop_at;

	int current_frame;

	HintVector position;
	ndec current_time;
	std::shared_ptr<HImage> himage;

	int num_frames;
	std::vector<HintRect> vector_frame_texture_rect;
	std::vector<ndec> vector_frame_time;
	std::vector<HintVector> vector_frame_position;

	HAnimationStatus status;
};

#endif