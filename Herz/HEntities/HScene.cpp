#include "HScene.h"

HScene::HScene() {
	loaded = false;
	loading = false;
	finished = false;
}

std::shared_ptr<HActor> HScene::search_hactor(const HintRect& area, std::string type) {
	for (std::shared_ptr<HActor>& hactor : vector_hactors) {
		if (collide_area_to_hactor(area, hactor->get_bounds()) && hactor->get_type() == type) {
			return hactor;
		}
	}

	return nullptr;
}

const std::vector<std::shared_ptr<HActor> >& HScene::search_hactors(const HintRect& area, std::string type) {
	vector_founded_hactors.clear();
	for (std::shared_ptr<HActor>& hactor : vector_hactors) {
		if (collide_area_to_hactor(area, hactor->get_bounds()) && hactor->get_type() == type) {
			vector_founded_hactors.push_back(hactor);
		}
	}
	
	return vector_founded_hactors;
}

const std::vector<std::shared_ptr<HActor> >& HScene::get_all_hactors(std::string type) {
	return vector_hactors;
}

std::shared_ptr<HCamera> HScene::get_hcamera() {
	return herz_interface->get_hcamera();
}

void HScene::start_load() {
	this->loading = true;
	std::function<void()> fun = [this](){
		this->load();
		this->loaded = true;
		this->loading = false;
	};
	hthread = HCore::s_hfactory()->make_hthread();
	hthread->initialize(fun);
	hthread->run();
}

void HScene::set_herz_interface(std::shared_ptr<HerzInterface> herzinterface) {
	this->herz_interface = herzinterface;
}
void HScene::finish() {
	finished = true;
}

bool HScene::is_loaded() {
	return loaded;
}
bool HScene::is_loading() {
	return loading;
}
bool HScene::is_finished() {
	return finished;
}
void HScene::add_hactor(std::shared_ptr<HActor> hactor) {
	hactor->set_hscene_interface(shared_from_this());
	vector_hactors.push_back(hactor);
}

// private
bool HScene::collide_area_to_hactor(const HintRect& area, const HintRect& hactor_bounds) {
	static bool collide_x;
	collide_x =
	area.get_y() < hactor_bounds.get_y() + hactor_bounds.get_height() &&
	area.get_y() + area.get_height() > hactor_bounds.get_y();

	if ( ! collide_x)
		return false;

	static bool collide_y;
	collide_y =
	area.get_x() < hactor_bounds.get_x() + hactor_bounds.get_width() &&
	area.get_x() + area.get_width() > hactor_bounds.get_x();

	if ( ! collide_y)
		return false;

	return true;
}