#include "HAnimator.h"

HAnimator::HAnimator() {
	
}

void HAnimator::load(std::shared_ptr<HNodeInfo> hnodeinfo_animator) {
	const std::vector<HNodeInfo>& vector_animations = hnodeinfo_animator->get_children("animation");

	for (const HNodeInfo& hnodeinfo_animation : vector_animations) {
		
		std::string name_hanimation = hnodeinfo_animation.get_attribute("name");
		
		std::shared_ptr<HAnimation> hanimation = std::make_shared<HAnimation>();
		hanimation->load(hnodeinfo_animation);

		add_hanimation(name_hanimation, hanimation);
	}
}

void HAnimator::add_hanimation(std::string name, std::shared_ptr<HAnimation> hanimation) {
	map_animations[ name ] = hanimation;

	if (this->hanimation == nullptr) {
		current_hanimation(name);
	}
}

void HAnimator::current_hanimation(std::string name) {
	if (map_animations[ name ] == nullptr) {
		std::cout << "Warning: animator doesn't have " << name << " animation" << std::endl;
		return;
	}

	if (hanimation != nullptr)
		hanimation->stop();

	hanimation = map_animations[ name ];
}

void HAnimator::update(const ndec& delta_time) {
	if (hanimation == nullptr)
		return;
	hanimation->update(delta_time);
}
void HAnimator::play() {
	if (hanimation == nullptr)
		return;
	hanimation->play();
}
void HAnimator::pause() {
	if (hanimation == nullptr)
		return;
	hanimation->pause();
}
void HAnimator::stop() {
	if (hanimation == nullptr)
		return;
	hanimation->stop();
}

void HAnimator::draw(std::shared_ptr<HImageSpace> himagespace, const HncVector& pivote_position) {
	if (hanimation == nullptr)
		return;

	hanimation->draw(himagespace, pivote_position.geti());
}