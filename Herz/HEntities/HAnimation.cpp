#include "HAnimation.h"


HAnimation::HAnimation() {
	status = HAnimationStatus::PLAY;
	bucle = true;
	num_frames = 0;
	current_frame = 0;
}

void HAnimation::load(std::shared_ptr<HNodeInfo> hnodeinfo_animation) {
	load(*hnodeinfo_animation.get());
}

void HAnimation::load(const HNodeInfo& hnodeinfo_animation) {
	std::string fps 	= hnodeinfo_animation.get_attribute("fps");
	std::string loop_at = hnodeinfo_animation.get_attribute("loop_at");

	this->fps 		= atoi(fps.c_str());
	this->loop_at 	= atoi(loop_at.c_str());

	const std::vector<HNodeInfo>& vector_instances = hnodeinfo_animation.get_children("instance");

	ndec frame_time = this->fps;
	frame_time = 1.0 / frame_time.get();

	for (const HNodeInfo& instance : vector_instances) {
		std::string name_instance 	= instance.get_attribute("name");
		int x_instance				= atoi(instance.get_attribute("x").c_str());
		int y_instance				= atoi(instance.get_attribute("y").c_str());
		
		std::shared_ptr<HImageInstance> himageinstance = HCore::s_hresources()->get_himageinstance(name_instance);

		set_texture(himageinstance->path_texture);
		add_frame(himageinstance->texture_rect, frame_time, HintVector(x_instance, y_instance));
	}
}

void HAnimation::set_texture(const std::string& texture_path) {
	if (himage != nullptr)
		return;
	himage = std::make_shared<HImage>(texture_path);
}
void HAnimation::add_frame(const HintRect& texture_rect, const ndec& frame_time, HintVector frame_position) {
	vector_frame_texture_rect.push_back(texture_rect);
	vector_frame_time.push_back(frame_time);
	vector_frame_position.push_back(frame_position);
	num_frames++;
}
void HAnimation::update(const ndec& delta_time) {
	if (status != HAnimationStatus::PLAY)
		return;

	current_time += delta_time;

	const ndec& frame_time = vector_frame_time[ current_frame ];
	
	if (current_time < frame_time)
		return;

	current_time -= frame_time;

	bool end_animation = current_frame + 1 == num_frames;
	if ( ! end_animation) {
		current_frame++;
		return;
	}

	if (bucle) {
		current_frame = 0;
		return;
	}

	stop();
}

void HAnimation::play() {
	status = HAnimationStatus::PLAY;
}
void HAnimation::pause() {
	status = HAnimationStatus::PAUSE;
}
void HAnimation::stop() {
	status = HAnimationStatus::STOP;
	current_frame = 0;
}

void HAnimation::set_position(const HintVector& position) {
	this->position = position;
}

void HAnimation::draw(std::shared_ptr<HImageSpace> himagespace, const HintVector& pivote_position) {
	const HintRect& frame_texture_rect = vector_frame_texture_rect[ current_frame ];
	himage->set_texture_rect(frame_texture_rect);
	himage->set_size(frame_texture_rect.get_size());

	const HintVector& frame_position = vector_frame_position[ current_frame ];

	static HintVector himage_position;
	himage_position.x = pivote_position.x + position.x + frame_position.x;
	himage_position.y = pivote_position.y + position.y + frame_position.y;
	himage->set_position(himage_position);

	himagespace->draw_himage(himage);
}
