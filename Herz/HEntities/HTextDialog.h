#ifndef _TEXTDIALOG_H_
#define _TEXTDIALOG_H_

#include <iostream>
#include <memory>
#include <queue>
#include <HEntities/HActor.h>
#include <HEntities/HLetter.h>
#include <HCore/HCore.h>
#include <HFactory/HCamera.h>

class HTextDialog : public HActor {
public:
	HTextDialog(const std::string& bitmap_font_path, const HintRect& area, ndec letter_delay = 0, bool relative = true);

	void update(const ndec& delta_time) override;
	void fixed_update(const ndec& delta_time) override;
	void draw(std::shared_ptr<HImageSpace> himagespace) override;

	void set_hscene_interface(std::shared_ptr<HSceneInterface> hsceneinterface) override;

	void next();
	void set_text(std::string text);
	void change_font(const std::string& bitmap_font_path);

	bool is_waiting();
	bool is_completed();

	bool is_kerning();
	void set_kerning(bool v);

private:
	bool active_kerning;

	int line_height;
	int space_width;

	HintVector cursor;
	HintRect area;
	ndec letter_delay;
	HintVector relative_position;
	bool relative;
	int current_index;

	std::shared_ptr<HCamera> hcamera;

	bool waiting;
	bool completed;
	bool active;
	std::string text;

	ndec current_time;
	int start_index;
	int stop_index;
	std::queue<int> queue_stop_index;
	std::shared_ptr<HNodeInfo> hnode_info;

	std::shared_ptr<HTexture> htexture;

	std::map<char, std::map<char, int> > map_kerning;
	std::map<char, int> map_letter;
	std::vector<HLetter> vector_letter;

	std::vector<HintVector> vector_position_hletter;

	std::map<std::string, std::shared_ptr<HImage> > map_himage;

	void init_bitmap_font(const std::string& bitmap_font_path);

	int get_kerning_distance(const char& A, const char& B);

	int get_word_width(const std::string& text, int id);
	const HLetter& get_hletter(char hletter);
	int get_stop_index();
};


#endif