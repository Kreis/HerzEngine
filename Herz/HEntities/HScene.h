#ifndef _HSCENE_H_
#define _HSCENE_H_

#include <memory>
#include <iostream>
#include <HBasic/ndec.h>
#include <HEntities/HerzInterface.h>
#include <HRender/HImageSpace.h>
#include <HEntities/HSceneInterface.h>
#include <HEntities/HActor.h>
#include <HCore/HCore.h>

class HScene : public HSceneInterface {
public:
	std::vector<std::string> vector_texture_priority;

	std::shared_ptr<HActor> search_hactor(const HintRect& area, std::string type = "") override;
	const std::vector<std::shared_ptr<HActor> >& search_hactors(const HintRect& area, std::string type = "") override;
	const std::vector<std::shared_ptr<HActor> >& get_all_hactors(std::string type = "") override;
	std::shared_ptr<HCamera> get_hcamera() override;

	
	virtual void fixed_update(const ndec& delta_time) = 0;
	virtual void update(const ndec& delta_time) = 0;
	virtual void draw(std::shared_ptr<HImageSpace> himagespace) = 0;
	

	HScene();

	void start_load();
	void set_herz_interface(std::shared_ptr<HerzInterface> herzinterface);
	void finish();
	bool is_loaded();
	bool is_loading();
	bool is_finished();

	void add_hactor(std::shared_ptr<HActor> hactor);

protected:
	std::shared_ptr<HerzInterface> herz_interface;
	std::vector<std::shared_ptr<HActor> > vector_hactors;
	std::vector<std::shared_ptr<HActor> > vector_founded_hactors;

private:
	bool collide_area_to_hactor(const HintRect& area, const HintRect& hactor_bounds);

	virtual void load() = 0;
	bool loaded;
	bool loading;
	bool finished;
	std::shared_ptr<HThread> hthread;
};

#endif