#ifndef _HMAPLAYER_H_
#define _HMAPLAYER_H_

#include <memory>
#include <iostream>
#include <HBasic/HintRect.h>
#include <HRender/HImageSpace.h>

class HMapLayer {
public:
	HMapLayer(const HintVector& map_position, const HintVector& grid_size, const HintVector& tile_size);

	void set_grid(std::unique_ptr<std::vector<int> > grid);

	void draw(
		std::shared_ptr<HImageSpace> himagespace,
		const HintRect& view_area,
		std::vector<std::shared_ptr<HImage> >& vector_himages);

private:
	HintVector grid_size;
	HintVector tile_size;
	HintVector position;


	std::unique_ptr<std::vector<int> > grid;
};

#endif