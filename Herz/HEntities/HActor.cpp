#include "HActor.h"

HActor::HActor() {
	type = "";
}

void HActor::set_hscene_interface(std::shared_ptr<HSceneInterface> hsceneinterface) {
	this->hsceneinterface = hsceneinterface;
}

const HintRect HActor::get_bounds() {
	return bounds.geti();
}

const std::string& HActor::get_type() {
	return type;
}

std::shared_ptr<HSceneInterface> HActor::get_hsceneinterface() {
	if (hsceneinterface == nullptr) {
		std::cout << "Error, HActor not relationed with HScene, use HScene::add_actor function" << std::endl;
	}
	return hsceneinterface;
}