#include "HMapLayer.h"

HMapLayer::HMapLayer(const HintVector& map_position, const HintVector& grid_size, const HintVector& tile_size) {
	this->position = map_position;
	this->grid_size = grid_size;
	this->tile_size = tile_size;
}

void HMapLayer::set_grid(std::unique_ptr<std::vector<int> > grid) {
	this->grid = std::move(grid);
}

void HMapLayer::draw(
	std::shared_ptr<HImageSpace> himagespace,
	const HintRect& view_area,
	std::vector<std::shared_ptr<HImage> >& vector_himages) {

	int initial_x = (view_area.get_x() - position.x) / tile_size.x;
	initial_x = initial_x < 0 ? 0 : initial_x;
	initial_x = initial_x >= grid_size.x ? grid_size.x : initial_x;
	int x = initial_x;

	int initial_y = (view_area.get_y() - position.y) / tile_size.y;
	initial_y = initial_y >= grid_size.y ? grid_size.y : initial_y;
	initial_y = initial_y < 0 ? 0 : initial_y;
	int y = initial_y;

	int x2 = (view_area.get_x() + view_area.get_width() + tile_size.x - position.x) / tile_size.x;
	x2 = x2 < 0 ? 0 : x2;
	x2 = x2 > grid_size.x ? grid_size.x : x2;

	int y2 = (view_area.get_y() + view_area.get_height() + tile_size.y - position.y) / tile_size.y;
	y2 = y2 < 0 ? 0 : y2;
	y2 = y2 > grid_size.y ? grid_size.y : y2;

	for (int i = x; i < x2; i++) {
		for (int j = y; j < y2; j++) {

			int id = (j * grid_size.x) + i;
			if (id < 0 || id >= grid->size())
				continue;
			int gid = grid->at( id );
			if (gid > 0) {
				gid--;

				static HintVector himage_position;
				himage_position.x = i * tile_size.x  +  position.x;
				himage_position.y = j * tile_size.y  +  position.y;
				vector_himages[ gid ]->set_position(himage_position);
				himagespace->draw_himage(vector_himages[ gid ]);
			}

		}
	}
}