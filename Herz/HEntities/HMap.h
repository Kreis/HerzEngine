#ifndef _HMAP_H_
#define _HMAP_H_

#include <memory>
#include <iostream>
#include <HCore/HCore.h>
#include <HRender/HImageSpace.h>
#include <HResources/HResources.h>
#include <HBasic/HintRect.h>
#include <HEntities/HMapLayer.h>
#include <stdlib.h>
#include <string>

class HMap {
public:
	HMap();
	void load(std::string path, HintVector position = HintVector());
	void draw(std::shared_ptr<HImageSpace> himagespace, const HintRect& view_area);

private:
	std::vector<std::shared_ptr<HImage> > vector_himages;
	std::vector<HMapLayer> vector_layers;

	HintVector global_tile_size;
	HintVector global_grid_size;

	HintVector himage_position;
};

#endif