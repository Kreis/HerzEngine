#include "HLetter.h"

HLetter::HLetter(char letter, int x, int y, int width, int height, int xoffset, int yoffset, int xadvance, std::string id_texture) {
	this->letter = letter;
	this->texture_rect.x() = x;
	this->texture_rect.y() = y;
	this->texture_rect.width() = width;
	this->texture_rect.height() = height;
	this->xoffset = xoffset;
	this->yoffset = yoffset;
	this->xadvance = xadvance;
	this->id_texture = id_texture;
}