#ifndef _HERZINTERFACE_H_
#define _HERZINTERFACE_H_

#include <memory>
#include <iostream>
#include <vector>

class HScene;
class HCamera;
class HerzInterface : public std::enable_shared_from_this<HerzInterface> {
public:
	virtual void load_hscene(std::shared_ptr<HScene> hscene) = 0;
	virtual std::shared_ptr<HCamera> get_hcamera() = 0;
};

#endif