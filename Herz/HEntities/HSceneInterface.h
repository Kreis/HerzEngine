#ifndef _HSCENEINTERFACE_H_
#define _HSCENEINTERFACE_H_

#include <memory>
#include <iostream>
#include <HBasic/HintRect.h>
#include <vector>

class HActor;
class HCamera;
class HSceneInterface : public std::enable_shared_from_this<HSceneInterface> {
public:
	virtual std::shared_ptr<HActor> search_hactor(const HintRect& area, std::string type = "") = 0;
	virtual const std::vector<std::shared_ptr<HActor> >& search_hactors(const HintRect& area, std::string type = "") = 0;
	virtual const std::vector<std::shared_ptr<HActor> >& get_all_hactors(std::string type = "") = 0;
	virtual std::shared_ptr<HCamera> get_hcamera() = 0;
};

#endif