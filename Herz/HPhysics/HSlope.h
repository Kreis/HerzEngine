#ifndef _HSLOPE_H_
#define _HSLOPE_H_

#include <memory>
#include <iostream>
#include <HBasic/HncVector.h>
#include <HPhysics/HRigidBody.h>

enum class HSlopeFace {
	FACE_LEFT,
	FACE_RIGHT
};

class HSlope {
public:
	HSlope(const HncVector& peek, const HncVector& tip);

	bool move_down(std::shared_ptr<HRigidBody>& hrigidbody);
	bool move_right(std::shared_ptr<HRigidBody>& hrigidbody);
	bool move_up(std::shared_ptr<HRigidBody>& hrigidbody);
	bool move_left(std::shared_ptr<HRigidBody>& hrigidbody);

	HintRect get_bounds();

private:
	ndec angle;
	ndec m;
	HSlopeFace face;

	HncVector peek;
	HncVector tip;
};

 #endif
