#ifndef _HRIGIDGRID_H_
#define _HRIGIDGRID_H_

#include <memory>
#include <iostream>
#include <vector>
#include <HBasic/HintVector.h>
#include <HPhysics/HRigidBody.h>

class HRigidGrid {
public:
	HRigidGrid();

	void set_grid(std::unique_ptr<std::vector<int> > grid, int tile_width, int tile_height, int columns);
	void set_position(const HintVector& grid_position);
	int type_tile(int x, int y);


	bool move_down(std::shared_ptr<HRigidBody>& hrigidbody);
	bool move_right(std::shared_ptr<HRigidBody>& hrigidbody);
	bool move_up(std::shared_ptr<HRigidBody>& hrigidbody);
	bool move_left(std::shared_ptr<HRigidBody>& hrigidbody);

private:

	bool p_move_down(const HncVector& position, const HintVector& size, HncVector& trying_position);
	bool p_move_right(const HncVector& position, const HintVector& size, HncVector& trying_position);
	bool p_move_up(const HncVector& position, const HintVector& size, HncVector& trying_position);
	bool p_move_left(const HncVector& position, const HintVector& size, HncVector& trying_position);

	HintVector grid_position;
	int tile_width;
	int tile_height;
	int columns;
	std::unique_ptr<std::vector<int> > grid;

	inline bool is_rigid(int x, int y);
};

#endif