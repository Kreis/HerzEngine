#include "HRigidBody.h"

HRigidBody::HRigidBody(HintRect rect_bound) {
	this->p_position.x 	= rect_bound.position().x;
	this->p_position.y 	= rect_bound.position().y;
	this->p_trying_position = this->p_position;
	this->p_size 		= rect_bound.size();

	dynamic				= true;
	
	active				= true;
	solid_side_left		= true;
	solid_side_right	= true;
	solid_side_up		= true;
	solid_side_down		= true;

	colliding_down 		= false;
	colliding_up 		= false;
}

HncVector& HRigidBody::trying_position() {
	return p_trying_position;
}
HintVector& HRigidBody::size() {
	return p_size;
}

void HRigidBody::set_absolute_position(const HncVector& position) {
	p_position = position;
	p_trying_position = position;
}

void HRigidBody::set_move_x(const ndec& x) {
	movement.x = x;
}
void HRigidBody::set_move_y(const ndec& y) {
	movement.y = y;
}

void HRigidBody::set_position_x(const ndec& x) {
	p_trying_position.x = x;
}
void HRigidBody::set_position_y(const ndec& y) {
	p_trying_position.y = y;
}

const ndec& HRigidBody::get_move_x() const {
	return movement.x;
}
const ndec& HRigidBody::get_move_y() const {
	return movement.y;
}

const HncVector& HRigidBody::get_position() const {
	return p_position;
}

const HintVector& HRigidBody::get_size() const {
	return p_size;
}

// private just for hpyshics
HncVector& HRigidBody::position() {
	return p_position;
}