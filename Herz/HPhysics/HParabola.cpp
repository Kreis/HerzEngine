#include "HParabola.h"

HParabola::HParabola() {
	alive = false;
}

void HParabola::initialize(
	std::shared_ptr<HRigidBody> hrigidbody,
	const ndec& time_to_peak,
	const ndec& height,
	const ndec& current_time,
	const HTypeParabola& htypeparabola,
	ndec limit_time
	) {
	alive = true;

	this->hrigidbody 		= hrigidbody;
	this->time_to_peak 		= time_to_peak;
	this->height 			= height;
	this->current_time 		= current_time;
	this->htypeparabola 	= htypeparabola;
	this->limit_time 		= limit_time;

	g = (height * -2) / (time_to_peak * time_to_peak);
	v0 = -(g * time_to_peak);

	switch (htypeparabola) {
		case HTypeParabola::VERTICAL:
			initial_position = hrigidbody->get_position().y - parabola_formula();
			break;
		case HTypeParabola::VERTICAL_INVERTED:
			initial_position = hrigidbody->get_position().y + parabola_formula();
			break;
		case HTypeParabola::HORIZONTAL:
			initial_position = hrigidbody->get_position().x - parabola_formula();
			break;
		case HTypeParabola::HORIZONTAL_INVERTED:
			initial_position = hrigidbody->get_position().x + parabola_formula();
			break;
	}
}

bool HParabola::update(const ndec& delta_time) {
	if ( ! alive) {
		return false;
	}
	if ( ! update_time(delta_time)) {
		alive = false;
		return false;
	}
	if (is_colliding()) {
		alive = false;
		return false;
	}

	static ndec parabola_result;
	parabola_result = parabola_formula();

	switch (htypeparabola) {
		case HTypeParabola::VERTICAL:
			hrigidbody->set_position_y(initial_position + parabola_result);
			break;
		case HTypeParabola::VERTICAL_INVERTED:
			hrigidbody->set_position_y(initial_position - parabola_result);
			break;
		case HTypeParabola::HORIZONTAL:
			hrigidbody->set_position_x(initial_position + parabola_result);
			break;
		case HTypeParabola::HORIZONTAL_INVERTED:
			hrigidbody->set_position_x(initial_position - parabola_result);
			break;
	}
	
	return true;
}

// private 
bool HParabola::update_time(const ndec& delta_time) {
	if (limit_time != -1 && current_time >= limit_time)
		return false;

	current_time += delta_time;

	if (limit_time != -1 && current_time >= limit_time)
		current_time = limit_time;

	return true;
}

bool HParabola::is_colliding() {
	static bool to_down;
	static bool to_up;
	static bool to_left;
	static bool to_right;

	to_down = 
	(htypeparabola == HTypeParabola::VERTICAL && current_time < time_to_peak) ||
	(htypeparabola == HTypeParabola::VERTICAL_INVERTED && current_time > time_to_peak);
	if (to_down)
		return hrigidbody->colliding_down;

	to_up =
	(htypeparabola == HTypeParabola::VERTICAL_INVERTED && current_time < time_to_peak) || 
	(htypeparabola == HTypeParabola::VERTICAL && current_time > time_to_peak);
	if (to_up)
		return hrigidbody->colliding_up;

	to_left =
	(htypeparabola == HTypeParabola::HORIZONTAL_INVERTED && current_time < time_to_peak) ||
	(htypeparabola == HTypeParabola::HORIZONTAL && current_time > time_to_peak);
	if (to_left)
		return hrigidbody->colliding_left;

	to_right =
	(htypeparabola == HTypeParabola::HORIZONTAL_INVERTED && current_time < time_to_peak) ||
	(htypeparabola == HTypeParabola::HORIZONTAL && current_time > time_to_peak);
	if (to_right)
		return hrigidbody->colliding_right;

	return false;
}

ndec HParabola::parabola_formula() {
	 return (v0 * current_time) + (g * 0.5) * (current_time * current_time);
}







