#ifndef _HPARABOLA_H_
#define _HPARABOLA_H_

#include <memory>
#include <iostream>
#include <HPhysics/HRigidBody.h>
#include <HBasic/ndec.h>

enum class HTypeParabola {
	VERTICAL,
	VERTICAL_INVERTED,
	HORIZONTAL,
	HORIZONTAL_INVERTED
};

class HParabola {
public:
	HParabola();

	void initialize(
		std::shared_ptr<HRigidBody> hrigidbody,
		const ndec& time_to_peak,
		const ndec& height,
		const ndec& current_time,
		const HTypeParabola& htypeparabola,
		ndec limit_time = -1
		);

	bool update(const ndec& delta_time);

private:
	std::shared_ptr<HRigidBody> hrigidbody;
	ndec initial_position;
	ndec time_to_peak;
	ndec height;
	ndec current_time;
	HTypeParabola htypeparabola;
	ndec limit_time;

	ndec v0;
	ndec g;

	bool update_time(const ndec& delta_time);
	bool is_colliding();

	ndec parabola_formula();

	bool alive;
};

#endif