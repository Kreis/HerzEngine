#include "HSlope.h"

HSlope::HSlope(const HncVector& peek, const HncVector& tip) {
	this->peek = peek;
	this->tip = tip;

	if (peek.x < tip.x) {
		face = HSlopeFace::FACE_RIGHT;
	} else {
		face = HSlopeFace::FACE_LEFT;
	}

	m = (peek.y - tip.y) / (peek.x - tip.x);
	m = m.abs();
}

bool HSlope::move_down(std::shared_ptr<HRigidBody>& hrigidbody) {
	
	const HncVector& position = hrigidbody->get_position();
	const HintVector& size = hrigidbody->get_size();

	HncVector& trying_position = hrigidbody->trying_position();

	if (trying_position.x + size.x <= tip.x.get() || position.x >= peek.x)
		return false;

	return false;
}

bool HSlope::move_right(std::shared_ptr<HRigidBody>& hrigidbody) {
	if (face == HSlopeFace::FACE_LEFT) {
		const HncVector& position = hrigidbody->get_position();
		const HintVector& size = hrigidbody->get_size();

		HncVector& trying_position = hrigidbody->trying_position();

		if (position.y + size.y <= tip.y.get() || position.y >= peek.y.get())
			return false;

		int slap_x = (position.y + size.y) / m.get();

		if (face == HSlopeFace::FACE_LEFT) {
			slap_x += tip.x.get();
		} else if (face == HSlopeFace::FACE_RIGHT) {
			// slap_x = (tip.x - slap_x) + peek.x.get();
		}

		bool pos_left = position.x < slap_x;
		bool try_left = trying_position.x < slap_x;

		bool collide_x = pos_left != try_left;
		if (collide_x)
			std::cout << "colliding slope" << std::endl;
		
		return collide_x;
	}

	return true;
}

bool HSlope::move_up(std::shared_ptr<HRigidBody>& hrigidbody) {
	return false;
}

bool HSlope::move_left(std::shared_ptr<HRigidBody>& hrigidbody) {
	return false;
}

HintRect HSlope::get_bounds() {

	if (face == HSlopeFace::FACE_LEFT) {
		return HintRect(tip.x.geti(), peek.y.geti(), peek.x - tip.x, tip.y - peek.y);
	} else {

	}
	
	return HintRect();
}




