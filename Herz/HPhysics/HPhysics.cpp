#include "HPhysics.h"
#include <HCore/HCore.h>

HPhysics::HPhysics() {
}

void HPhysics::add_hrigidbody(std::shared_ptr<HRigidBody> hrigidbody) {
	if (hrigidbody->dynamic)
		vector_dynamic_bodies.push_back(hrigidbody);
	else
		vector_static_bodies.push_back(hrigidbody);
}

void HPhysics::update() {
	
	calcule_trying_moves();

	for (std::shared_ptr<HRigidBody>& body : vector_dynamic_bodies) {
		ndec dif_x = body->trying_position().x - body->position().x;

		if (dif_x > 0)
			move_right(body);
		if (dif_x < 0)
			move_left(body);

		ndec dif_y = body->trying_position().y - body->position().y;

		if (dif_y > 0)
			move_down(body);
		if (dif_y < 0)
			move_up(body);

	}

}

void HPhysics::add_rigid_map(std::string path_map, HintVector map_position) {
	std::shared_ptr<HNodeInfo> hnode_map = HCore::s_hresources()->get_hnodeinfo_map(path_map);

	int tile_width 	= atoi(hnode_map->get_attribute("tilewidth").c_str());
	int tile_height	= atoi(hnode_map->get_attribute("tileheight").c_str());
	int columns 	= atoi(hnode_map->get_attribute("width").c_str());


	const std::vector<HNodeInfo>& vector_layer = hnode_map->get_children("layer");


	for (const HNodeInfo& layer : vector_layer) {

		std::string name = layer.get_attribute("name");

		if (name != "rigid")
			continue;

		int layer_width = atoi(layer.get_attribute("width").c_str());
		int layer_height = atoi(layer.get_attribute("height").c_str());

		std::string grid_string = layer.get_value();
		
		std::unique_ptr<std::vector<int> > grid_rigid = std::unique_ptr<std::vector<int> >(new std::vector<int>());
		HCore::s_hresources()->csv_to_vector(grid_string, *grid_rigid.get());

		int current_hrigidgrid = vector_hrigidgrid.size();
		vector_hrigidgrid.push_back(HRigidGrid());
		vector_hrigidgrid[ current_hrigidgrid ].set_grid(std::move(grid_rigid), tile_width, tile_height, columns);
		vector_hrigidgrid[ current_hrigidgrid ].set_position(map_position);
		break;
	}
}

void HPhysics::add_hslope(std::shared_ptr<HSlope> hslope) {
	vector_hslope.push_back(hslope);
}

std::vector<std::shared_ptr<HRigidBody> >& HPhysics::get_static_bodies() {
	return vector_static_bodies;
}

std::vector<std::shared_ptr<HRigidBody> >& HPhysics::get_dynamic_bodies() {
	return vector_dynamic_bodies;
}

std::vector<std::shared_ptr<HSlope> >& HPhysics::get_hslopes() {
	return vector_hslope;
}

// private
void HPhysics::calcule_trying_moves() {
	for (std::shared_ptr<HRigidBody>& body : vector_dynamic_bodies) {

		const ndec& move_x = body->get_move_x();
		if (move_x != 0) {
			body->trying_position().x += move_x;
			body->set_move_x(0);
		}
		const ndec& move_y = body->get_move_y();
		if (move_y != 0) {
			body->trying_position().y += move_y;
			body->set_move_y(0);
		}
	}
}

void HPhysics::move_right(std::shared_ptr<HRigidBody> body) {
	body->colliding_left = false;


	for (HRigidGrid& hrigidgrid : vector_hrigidgrid) {
		if (hrigidgrid.move_right(body)) {
			body->colliding_right = true;
			break;
		}
	}

	for (std::shared_ptr<HSlope>& hslope : vector_hslope) {
		if (hslope->move_right(body)) {
			body->colliding_right = true;
		}
	}

	for (std::shared_ptr<HRigidBody>& other : vector_static_bodies) {
		if (collide_x(body, other)) {
			body->trying_position().x = other->position().x - body->size().x;
		}
	}

	if (body->position().x != body->trying_position().x)
		body->colliding_right = false;

	body->position().x = body->trying_position().x;
}
void HPhysics::move_down(std::shared_ptr<HRigidBody> body) {

	for (HRigidGrid& hrigidgrid : vector_hrigidgrid) {
		if (hrigidgrid.move_down(body)) {
			body->colliding_down = true;
			break;	
		}
	}
	
	body->colliding_up = false;
	for (std::shared_ptr<HRigidBody>& other : vector_static_bodies) {
		if (collide_y(body, other)) {
			body->trying_position().y = other->position().y - body->size().y;
			body->colliding_down = true;
		}
	}

	if (body->position().y != body->trying_position().y)
		body->colliding_down = false;

	body->position().y = body->trying_position().y;

}
void HPhysics::move_up(std::shared_ptr<HRigidBody> body) {

	for (HRigidGrid& hrigidgrid : vector_hrigidgrid) {
		if (hrigidgrid.move_up(body)) {
			body->colliding_up = true;
			break;
		}
	}

	body->colliding_down = false;
	for (std::shared_ptr<HRigidBody>& other : vector_static_bodies) {
		if (collide_y(body, other)) {
			body->trying_position().y = other->position().y + other->size().y;
		}
	}

	if (body->position().y != body->trying_position().y)
		body->colliding_up = false;

	body->position().y = body->trying_position().y;
}
void HPhysics::move_left(std::shared_ptr<HRigidBody> body) {

	for (HRigidGrid& hrigidgrid : vector_hrigidgrid) {
		if (hrigidgrid.move_left(body)) {
			body->colliding_left = true;
			break;
		}
	}

	body->colliding_right = false;
	for (std::shared_ptr<HRigidBody>& other : vector_static_bodies) {
		if (collide_x(body, other)) {
			body->trying_position().x = other->position().x + other->size().x;
		}
	}

	if (body->position().x != body->trying_position().x)
		body->colliding_left = false;

	body->position().x = body->trying_position().x;
}

bool HPhysics::collide_x(const std::shared_ptr<HRigidBody>& body, const std::shared_ptr<HRigidBody>& other) const {
	static ndec body_left;
	static ndec body_right;
	if (body->position().x < body->trying_position().x) {
		body_left = body->position().x;
		body_right = body->trying_position().x + body->size().x;
	} else {
		body_left = body->trying_position().x;
		body_right = body->position().x + body->size().x;
	}
	
	bool collide_x = 
	body_left < other->position().x + other->size().x && 
	other->position().x < body_right;

	if ( ! collide_x)
		return false;

	bool collide_y = 
	body->position().y < other->position().y + other->size().y && 
	 other->position().y < body->position().y + body->size().y;

	if ( ! collide_y)
		return false;

	return true;
}

bool HPhysics::collide_y(const std::shared_ptr<HRigidBody>& body, const std::shared_ptr<HRigidBody>& other) const {
	bool collide_x = 
	body->position().x < other->position().x + other->size().x && 
	other->position().x < body->position().x + body->size().x;

	if ( ! collide_x)
		return false;

	static ndec body_up;
	static ndec body_down;
	if (body->position().y < body->trying_position().y) {
		body_up = body->position().y;
		body_down = body->trying_position().y + body->size().y;
	} else {
		body_up = body->trying_position().y;
		body_down = body->position().y + body->size().y;
	}

	bool collide_y = 
	body_up < other->position().y + other->size().y && 
	 other->position().y < body_down;

	if ( ! collide_y)
		return false;

	return true;
}








