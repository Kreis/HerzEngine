#include "HRigidGrid.h"

HRigidGrid::HRigidGrid() {
	
}

void HRigidGrid::set_grid(
	std::unique_ptr<std::vector<int> > grid,
	int tile_width,
	int tile_height,
	int columns) {

	this->grid = std::move(grid);
	this->tile_width = tile_width;
	this->tile_height = tile_height;

	this->columns = columns;
}

void HRigidGrid::set_position(const HintVector& grid_position) {
	this->grid_position = grid_position;
}

int HRigidGrid::type_tile(int x, int y) {
	return 0;
}


bool HRigidGrid::move_down(std::shared_ptr<HRigidBody>& hrigidbody) {
	if (grid->empty()) {
		std::cout << "Error: HRigidGrid doesn't have any grid to check collides" << std::endl;
		return false;
	}

	const HncVector& position = hrigidbody->get_position();
	const HintVector& size = hrigidbody->get_size();

	HncVector& trying_position = hrigidbody->trying_position();

	return p_move_down(position, size, trying_position);
}

bool HRigidGrid::move_right(std::shared_ptr<HRigidBody>& hrigidbody) {
	if (grid->empty()) {
		std::cout << "Error: HRigidGrid doesn't have any grid to check collides" << std::endl;
		return false;
	}

	const HncVector& position = hrigidbody->get_position();
	const HintVector& size = hrigidbody->get_size();

	HncVector& trying_position = hrigidbody->trying_position();

	return p_move_right(position, size, trying_position);
}

bool HRigidGrid::move_up(std::shared_ptr<HRigidBody>& hrigidbody) {
	if (grid->empty()) {
		std::cout << "Error: HRigidGrid doesn't have any grid to check collides" << std::endl;
		return false;
	}

	const HncVector& position = hrigidbody->get_position();
	const HintVector& size = hrigidbody->get_size();

	HncVector& trying_position = hrigidbody->trying_position();

	return p_move_up(position, size, trying_position);
}

bool HRigidGrid::move_left(std::shared_ptr<HRigidBody>& hrigidbody) {
	if (grid->empty()) {
		std::cout << "Error: HRigidGrid doesn't have any grid to check collides" << std::endl;
		return false;
	}

	const HncVector& position = hrigidbody->get_position();
	const HintVector& size = hrigidbody->get_size();

	HncVector& trying_position = hrigidbody->trying_position();

	return p_move_left(position, size, trying_position);
}

// private
bool HRigidGrid::is_rigid(int x, int y) {
	static int _x, _y, _f;

	if (x < 0 || y < 0)
		return false;

	_x = x / tile_width;
	_y = y / tile_height;

	if (_x >= columns)
		return false;

	_f = _y*columns + _x;
	if (_f < 0 || _f >= grid->size())
		return false;
	return grid->at( _f ) != 0;
}

bool HRigidGrid::p_move_down(const HncVector& position, const HintVector& size, HncVector& trying_position) {
	static int x, w;
	x = position.x.geti() - grid_position.x;
	w = size.x;

	static int p, tp;
	p = position.y.geti() + size.y - grid_position.y;
	tp = trying_position.y.geti() + size.y - grid_position.y;

	static int check;
	check = p + 1;

	bool collide = false;
	while (true) {

		for (int i = x; i < x + w; i += tile_width) {
			if (is_rigid(i, check)) {
				trying_position.y = (check / tile_height) * tile_height - size.y + grid_position.y;
				collide = true;
				break;
			}
		}
		if (is_rigid(x + w - 1, check)) {
			trying_position.y = (check / tile_height) * tile_height - size.y + grid_position.y;
			collide = true;
			break;
		}


		if (check == tp)
			break;
		check = check + tile_height > tp ? tp : check + tile_height;
	}

	return collide;
}

bool HRigidGrid::p_move_right(const HncVector& position, const HintVector& size, HncVector& trying_position) {
	static int y, h;
	y = position.y.geti() - grid_position.y;
	h = size.y;

	static int p, tp;
	p = position.x.geti() + size.x - grid_position.x;
	tp = trying_position.x.geti() + size.x - grid_position.x;

	static int check;
	check = p + 1;

	bool collide = false;
	while (true) {

		for (int i = y; i < y + h; i += tile_height) {
			if (is_rigid(check, i)) {
				trying_position.x = (check / tile_width) * tile_width - size.x + grid_position.x;
				collide = true;
				break;
			}
		}
		if (is_rigid(check, y + h - 1)) {
			trying_position.x = (check / tile_width) * tile_width - size.x + grid_position.x;
			collide = true;
			break;
		}


		if (check == tp)
			break;
		check = check + tile_width > tp ? tp : check + tile_width;
	}

	return collide;
}

bool HRigidGrid::p_move_up(const HncVector& position, const HintVector& size, HncVector& trying_position) {
	static int x, w;
	x = position.x.geti() - grid_position.x;
	w = size.x;

	static int p, tp;
	p = position.y.geti() - grid_position.y;
	tp = trying_position.y.geti() - grid_position.y;

	static int check;
	check = p + 1;

	bool collide = false;
	while (true) {

		for (int i = x; i < x + w; i += tile_width) {

			if (is_rigid(i, check)) {
				trying_position.y = (check / tile_height) * tile_height + tile_height + grid_position.y;
				collide = true;
				break;
			}
		}
		if (is_rigid(x + w - 1, check)) {
			trying_position.y = (check / tile_height) * tile_height + tile_height + grid_position.y;
			collide = true;
			break;
		}


		if (check == tp)
			break;
		check = check + tile_height > tp ? tp : check + tile_height;
	}

	return collide;
}

bool HRigidGrid::p_move_left(const HncVector& position, const HintVector& size, HncVector& trying_position) {
	static int y, h;
	y = position.y.geti() - grid_position.y;
	h = size.y;

	static int p, tp;
	p = position.x.geti() - grid_position.x;
	tp = trying_position.x.geti() - grid_position.x;

	static int check;
	check = p + 1;

	bool collide = false;
	while (true) {

		for (int i = y; i < y + h; i += tile_height) {

			if (is_rigid(check, i)) {
				trying_position.x = (check / tile_width) * tile_width + tile_width + grid_position.x;
				collide = true;
				break;
			}
		}
		if (is_rigid(check, y + h - 1)) {
			trying_position.x = (check / tile_width) * tile_width + tile_width + grid_position.x;
			collide = true;
			break;
		}


		if (check == tp)
			break;
		check = check + tile_width > tp ? tp : check + tile_width;
	}

	return collide;
}






