#ifndef _HPHYSICS_H_
#define _HPHYSICS_H_

#include <memory>
#include <iostream>
#include <vector>
#include <HPhysics/HRigidBody.h>
#include <HPhysics/HRigidGrid.h>
#include <HPhysics/HRigidJail.h>
#include <HPhysics/HSlope.h>
#include <HBasic/ndec.h>
#include <HBasic/HintVector.h>

class HPhysics {
friend class HCore;
public:
	void add_hrigidbody(std::shared_ptr<HRigidBody> hrigidbody);
	void add_hslope(std::shared_ptr<HSlope> hslope);
	void update();

	void add_rigid_map(std::string path_map, HintVector map_position = HintVector());

	std::vector<std::shared_ptr<HRigidBody> >& get_static_bodies();
	std::vector<std::shared_ptr<HRigidBody> >& get_dynamic_bodies();
	std::vector<std::shared_ptr<HSlope> >& get_hslopes();

private:
	HPhysics();

	std::vector<HRigidGrid> vector_hrigidgrid;
	std::vector<std::shared_ptr<HSlope>> vector_hslope;

	std::vector<std::shared_ptr<HRigidBody> > vector_static_bodies;
	std::vector<std::shared_ptr<HRigidBody> > vector_dynamic_bodies;

	void calcule_trying_moves();

	void move_right(std::shared_ptr<HRigidBody> body);
	void move_down(std::shared_ptr<HRigidBody> body);
	void move_up(std::shared_ptr<HRigidBody> body);
	void move_left(std::shared_ptr<HRigidBody> body);

	inline bool collide_x(const std::shared_ptr<HRigidBody>& body, const std::shared_ptr<HRigidBody>& other) const;
	inline bool collide_y(const std::shared_ptr<HRigidBody>& body, const std::shared_ptr<HRigidBody>& other) const;
};

#endif