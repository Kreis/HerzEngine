#ifndef _HRIGIDBODY_H_
#define _HRIGIDBODY_H_

#include <memory>
#include <iostream>
#include <HBasic/HintRect.h>
#include <HBasic/ndec.h>
#include <HBasic/HncVector.h>

class HRigidBody {
	friend class HPhysics;
public:
	HRigidBody(HintRect rect_bound);

	HncVector& trying_position();
	HintVector& size();
	
	bool dynamic;
	bool active;
	bool solid_side_left;
	bool solid_side_right;
	bool solid_side_up;
	bool solid_side_down;

	bool colliding_down;
	bool colliding_up;
	bool colliding_left;
	bool colliding_right;

	void set_absolute_position(const HncVector& position);
	
	void set_move_x(const ndec& x);
	void set_move_y(const ndec& y);

	void set_position_x(const ndec& x);
	void set_position_y(const ndec& y);

	const ndec& get_move_x() const;
	const ndec& get_move_y() const;

	const HncVector& get_position() const;
	const HintVector& get_size() const;
	
private:
	HncVector& position();

	HncVector p_position;
	HncVector p_trying_position;
	HintVector p_size;

	HncVector movement;
};

#endif