#ifndef _HNCRECT_H_
#define _HNCRECT_H_

#include <HBasic/ndec.h>
#include <HBasic/HintRect.h>
#include <HBasic/HncVector.h>

class HncRect {
public:
	HncRect(ndec x = 0, ndec y = 0, ndec width = 0, ndec height = 0) {
		p_position.x = x;
		p_position.y = y;
		p_size.x = width;
		p_size.y = height;
	}

	HintRect geti() const {
		return HintRect(
			p_position.x.geti(),
			p_position.y.geti(),
			p_size.x.geti(),
			p_size.y.geti());
	}

	ndec& x() {
		return p_position.x;
	}
	ndec& y() {
		return p_position.y;
	}
	ndec& width() {
		return p_size.x;
	}
	ndec& height() {
		return p_size.y;
	}
	HncVector& position() {
		return p_position;
	}
	HncVector& size() {
		return p_size;
	}

	const ndec& get_x() const {
		return p_position.x;
	}
	const ndec& get_y() const {
		return p_position.y;
	}
	const ndec& get_width() const {
		return p_size.x;
	}
	const ndec& get_height() const {
		return p_size.y;
	}
	const HncVector& get_position() const {
		return p_position;
	}
	const HncVector& get_size() const {
		return p_size;
	}

private:
	HncVector p_position;
	HncVector p_size;
};

#endif