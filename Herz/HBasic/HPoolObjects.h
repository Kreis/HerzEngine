#ifndef _HPOOLOBJECTS_H_
#define _HPOOLOBJECTS_H_

#include <stack>
#include <vector>

template <class T>
class HPoolObjects {
public:
	virtual ~HPoolObjects() {
		for (int i = 0; i < vec.size(); i++)
			delete vec[ i ];
	}
	void preload(int n) {
		for (int i = 0; i < n; i++)
			new_T();
	}
	void push(T* elem) {
		stack.push(elem);
	}
	T* pop() {
		if (stack.empty())
			new_T();

		T* elem = stack.top();
		stack.pop();
		return elem;
	}
	int size() {
		return stack.size();
	}
	void free_memory() {
		while ( ! stack.empty())
			stack.pop();

		for (int i = 0; i < vec.size(); i++) {
			delete vec[ i ];
		}
		vec.clear();
	}
private:
	std::stack<T*> stack;
	std::vector<T*> vec;

	void new_T() {
		T* newT = new T();
		stack.push(newT);
		vec.push_back(newT);
	}
};

#endif