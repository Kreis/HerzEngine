#ifndef _HNCVECTOR_H_
#define _HNCVECTOR_H_

#include <memory>
#include <iostream>
#include <HBasic/ndec.h>
#include <HBasic/HintVector.h>

class HncVector {
public:
	HncVector(ndec x = 0, ndec y = 0) {
		this->x = x;
		this->y = y;
	}
	HintVector geti() const {
		return HintVector(x.geti(), y.geti());
	}
	ndec x;
	ndec y;

	bool operator==(const HncVector& hncvector) const {
		return x == hncvector.x && y == hncvector.y;
	}

	bool operator!=(const HncVector& hncvector) const {
		return x != hncvector.x && y != hncvector.y;
	}

	HncVector operator+(const HncVector& hncvector) const {
		return HncVector(x + hncvector.x, y + hncvector.y);
	}

	void operator+=(const HncVector& hncvector) {
		x += hncvector.x;
		y += hncvector.y;
	}

	HncVector operator-(const HncVector& hncvector) const {
		return HncVector(x - hncvector.x, y - hncvector.y);
	}

	void operator-=(const HncVector& hncvector) {
		x -= hncvector.x;
		y -= hncvector.y;
	}

	HncVector operator*(const HncVector& hncvector) const {
		return HncVector(x * hncvector.x, y * hncvector.y);
	}

	void operator*=(const HncVector& hncvector) {
		x *= hncvector.x;
		y *= hncvector.y;
	}

	HncVector operator/(const HncVector& hncvector) const {
		return HncVector(x / hncvector.x, y / hncvector.y);
	}

	void operator/=(const HncVector& hncvector) {
		x /= hncvector.x;
		y /= hncvector.y;
	}
};

#endif
