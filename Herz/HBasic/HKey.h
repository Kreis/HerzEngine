#ifndef _HKEY_H_
#define _HKEY_H_

enum HKey {
	UP = 0,
	DOWN = 1,
	LEFT = 2,
	RIGHT = 3,
	ESC = 4,
	EMPTY = 5,
	
	A = 6,
	S = 7,


	COUNT = 8
};

#endif