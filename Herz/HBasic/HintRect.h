#ifndef _HINTRECT_H_
#define _HINTRECT_H_

#include <iostream>
#include <HBasic/HintVector.h>

class HintRect {
public:
	HintRect(int x = 0, int y = 0, int width = 0, int height = 0) {
		p_position.x = x;
		p_position.y = y;
		p_size.x = width;
		p_size.y = height;
	}

	int& x() {
		return p_position.x;
	}
	int& y() {
		return p_position.y;
	}
	int& width() {
		return p_size.x;
	}
	int& height() {
		return p_size.y;
	}
	HintVector& position() {
		return p_position;
	}
	HintVector& size() {
		return p_size;
	}

	const int& get_x() const {
		return p_position.x;
	}
	const int& get_y() const {
		return p_position.y;
	}
	const int& get_width() const {
		return p_size.x;
	}
	const int& get_height() const {
		return p_size.y;
	}
	const HintVector& get_position() const {
		return p_position;
	}
	const HintVector& get_size() const {
		return p_size;
	}

	bool operator==(const HintRect& hintrect) const {
		return p_position == hintrect.get_position() && p_size == hintrect.get_size();
	}

	bool operator!=(const HintRect& hintrect) const {
		return p_position != hintrect.get_position() || p_size != hintrect.get_size();
	}

private:
	HintVector p_position;
	HintVector p_size;
};

#endif