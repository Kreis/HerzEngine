#ifndef NDEC_H_
#define NDEC_H_

class ndec {
public:
	ndec() {
		val = 0;
	}
	ndec(double v) {
		val = format(v);
	}

	double abs() const {
		if (val >= 0)
			return format(val);
		return format(val) * ( -1 );
	}

	double operator+(const double& v) const {
		return format(val + v);
	}
	double operator+(ndec v) const {
		return format(val + v.get());
	}
	double operator+=(double v) {
		val = format(val + v);
		return val;
	}
	double operator+=(ndec v) {
		val = format(val + v.get());
		return val;
	}
	double operator-(double v) const {
		return format(val - v);
	}
	double operator-(ndec v) const {
		return format(val - v.get());
	}
	double operator-=(double v) {
		val = format(val - v);
		return val;
	}
	double operator-=(ndec v) {
		val = format(val - v.get());
		return val;
	}
	double operator*(double v) const {
		return format(val * v);
	}
	double operator*(ndec v) const {
		return format(val * v.get());
	}
	double operator*=(double v) {
		val = format(val * v);
		return val;
	}
	double operator*=(ndec v) {
		val = format(val * v.get());
		return val;
	}
	double operator/(double v) const {
		return format(val / v);
	}
	double operator/(ndec v) const {
		return format(val / v.get());
	}
	double operator/=(double v) {
		val = format(val / v);
		return val;
	}
	double operator/=(ndec v) {
		val = format(val / v.get());
		return val;
	}
	double operator=(double v) {
		val = format(v);
		return val;
	}
	double operator=(ndec v) {
		val = v.get();
		return val;
	}

	// booleans
	bool operator==(double v) const {
		return val == v;
	}
	bool operator==(ndec v) const {
		return val == v.get();
	}
	bool operator<(double v) const {
		return val < v;
	}
	bool operator<(ndec v) const {
		return val < v.get();
	}
	bool operator<=(double v) const {
		return val <= v;
	}
	bool operator<=(ndec v) const {
		return val <= v.get();
	}
	bool operator>(double v) const {
		return val > v;
	}
	bool operator>(ndec v) const {
		return val > v.get();
	}
	bool operator>=(double v) const {
		return val >= v;
	}
	bool operator>=(ndec v) const {
		return val >= v.get();
	}
	bool operator!=(double v) const {
		return val != v;
	}
	bool operator!=(ndec v) const {
		return val != v.get();
	}

	double get() const {
		return format(val);
	}
	int geti() const {
		return (int)val;
	}


private:
	double val;
	
	double format(double v) const {
		double vd, vd2;
		long vl;
		int real;
	
		real = v;
		vd = v - real;
		vl = (long)(vd * 1000000);
		vd2 = vl;
		return real + (vd2 / 1000000);
	}
};


#endif