#ifndef _HINTVECTOR_H_
#define _HINTVECTOR_H_

#include <memory>
#include <iostream>

class HintVector {
public:
	HintVector(int x = 0, int y = 0) {
		this->x = x;
		this->y = y;
	}

	int x;
	int y;

	bool operator==(const HintVector& hintvector) const {
		return x == hintvector.x && y == hintvector.y;
	}

	bool operator!=(const HintVector& hintvector) const {
		return x != hintvector.x || y != hintvector.y;
	}

	HintVector operator+(const HintVector& hintvector) const {
		return HintVector(x + hintvector.x, y + hintvector.y);
	}

	HintVector& operator+=(const HintVector& hintvector) {
		x += hintvector.x;
		y += hintvector.y;
		return *this;
	}

	HintVector operator-(const HintVector& hintvector) const {
		return HintVector(x - hintvector.x, y - hintvector.y);
	}

	HintVector& operator-=(const HintVector& hintvector) {
		x -= hintvector.x;
		y -= hintvector.y;
		return *this;
	}

	HintVector operator*(const HintVector& hintvector) const {
		return HintVector(x * hintvector.x, y * hintvector.y);
	}

	HintVector& operator*=(const HintVector& hintvector) {
		x *= hintvector.x;
		y *= hintvector.y;
		return *this;
	}

	HintVector operator/(const HintVector& hintvector) const {
		return HintVector(x / hintvector.x, y / hintvector.y);
	}

	HintVector& operator/=(const HintVector& hintvector) {
		x /= hintvector.x;
		y /= hintvector.y;
		return *this;
	}
};

#endif