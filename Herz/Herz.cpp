#include "Herz.h"

Herz::Herz(
		int width,
		int height,
		std::string title,
		int frame_rate,
		bool fullscreen) {
	
	hwindow = std::make_shared<HWindow>(width, height, title, frame_rate, fullscreen);
	htimer = HCore::s_hfactory()->make_htimer();
	fixed_time = ndec( 1 ) / ndec( frame_rate );
	himagespace = std::make_shared<HImageSpace>();
	this->fullscreen = fullscreen;
	new_hscene = nullptr;
	vector_texture_priority = nullptr;
	debug_mode = 0;
	HCore::s_hlscript()->export_var("debug_mode", debug_mode);
}

void Herz::start(std::shared_ptr<HScene> hscene) {

	load_hscene(hscene);

	while (true) {

		if (new_hscene != nullptr && ! new_hscene->is_loading()) {
			if ( ! new_hscene->is_loaded())
				new_hscene->start_load();

			bool last_hscene_can_replaced = current_hscene == nullptr || current_hscene->is_finished();
			if (new_hscene->is_loaded() && last_hscene_can_replaced) {
				current_hscene = new_hscene;
				new_hscene = nullptr;

				himagespace->reset();
				himagespace->set_texture_priority(current_hscene->vector_texture_priority);
			}
		}
		
		hwindow->update_input();

		if ( ! hwindow->is_open())
			break;

		if (HCore::s_hinput()->is_typed(HKey::UP) && HCore::s_hinput()->is_shift()) {
            fullscreen = !fullscreen;
            hwindow->set_fullscreen(fullscreen);
        }

		const ndec& delta_time = htimer->get_delta_time();
        
        if (debug_mode) {
        	hwindow->draw(himagespace, true, false);
        	hwindow->draw(get_draw_fps(), delta_time);
        	hwindow->display();
        } else {
        	hwindow->draw(himagespace);
        }


        if (current_hscene != nullptr && current_hscene->is_loaded())
			current_hscene->fixed_update(fixed_time);

		if (current_hscene != nullptr && current_hscene->is_loaded())
			current_hscene->update(delta_time);
		
		if (current_hscene != nullptr && current_hscene->is_loaded())
			current_hscene->draw(himagespace);

	}

}

void Herz::load_hscene(std::shared_ptr<HScene> hscene) {
	if (new_hscene != nullptr)
		return;

	new_hscene = hscene;
	if (new_hscene == nullptr)
		return;

	new_hscene->set_herz_interface(shared_from_this());
}

std::shared_ptr<HCamera> Herz::get_hcamera() {
	return hwindow->get_hcamera();
}

std::shared_ptr<HDrawFPS> Herz::get_draw_fps() {
	if (draw_fps != nullptr) {
		return draw_fps;
	}

	draw_fps = HCore::s_hfactory()->make_hdrawfps("arial.ttf");
	return draw_fps;
}