#include "HResources.h"
#include <HCore/HCore.h>

HResources::HResources() {

}

void HResources::add_atlas(std::string path) {
	XMLDocument xml;

	XMLError eResult = xml.LoadFile(path.c_str());

	if (eResult != XML_SUCCESS) {
		std::cout << "Error: file " << path.c_str() << " not found" << std::endl;
		return;
	}

	XMLElement* atlas_element = xml.FirstChildElement("atlas");
	if (atlas_element == nullptr) {
		std::cout << "Error: bad structure of atlas" << std::endl;
		return;
	}

	const char* _name_atlas = atlas_element->Attribute("name");
	const char* _src_atlas 	= atlas_element->Attribute("src");

	if (_name_atlas == nullptr || _src_atlas == nullptr) {
		std::cout << "Error: bad structure of atlas" << std::endl;
		return;
	}

	XMLElement* instance_element = atlas_element->FirstChildElement("instance");
	while (instance_element != nullptr) {

		const char* _name_instance 		= instance_element->Attribute("name");
		const char* _x_instance 		= instance_element->Attribute("x");
		const char* _y_instance 		= instance_element->Attribute("y");
		const char* _width_instance 	= instance_element->Attribute("width");
		const char* _height_instance 	= instance_element->Attribute("height");

		if (_name_instance == nullptr ||
			_x_instance == nullptr ||
			_y_instance == nullptr ||
			_width_instance == nullptr ||
			_height_instance == nullptr
			) {
			std::cout << "Error: bad structure of atlas" << std::endl;
			return;		
		}
		HintRect texture_coord(atoi(_x_instance), atoi(_y_instance), atoi(_width_instance), atoi(_height_instance));
		std::shared_ptr<HImageInstance> himageinstance = std::make_shared<HImageInstance>(_src_atlas, texture_coord);

		map_himageinstance[ _name_instance ] = himageinstance;

		instance_element = instance_element->NextSiblingElement("instance");
	}
}

std::shared_ptr<HTexture> HResources::get_htexture(const std::string& texture_path) {
	
	if (map_htextures[ texture_path ] != nullptr) {
		return map_htextures[ texture_path ];
	}

	std::shared_ptr<HTexture> htexture = HCore::s_hfactory()->make_htexture();
	htexture->load_from_file(texture_path);

	map_htextures[ texture_path ] = htexture;

	return htexture;
}

std::shared_ptr<HSound> HResources::get_hsound(const std::string& sound_path) {
	if (map_hsounds[ sound_path ] != nullptr) {
		return map_hsounds[ sound_path ];
	}

	std::shared_ptr<HSound> hsound = HCore::s_hfactory()->make_hsound();
	hsound->load_from_file(sound_path);

	map_hsounds[ sound_path ] = hsound;

	return hsound;
}

std::shared_ptr<HNodeInfo> HResources::get_hnodeinfo_animation(const std::string& animation_path) {
	XMLDocument xml;

	XMLError eResult = xml.LoadFile(animation_path.c_str());

	if (eResult != XML_SUCCESS) {
		std::cout << "Error: file " << animation_path.c_str() << " not found" << std::endl;
		return nullptr;
	}

	XMLElement* animator_element = xml.FirstChildElement("animator");
	if (animator_element == nullptr) {
		std::cout << "Error: bad structure of animator" << std::endl;
		return nullptr;
	}

	std::shared_ptr<HNodeInfo> hnode_root = std::make_shared<HNodeInfo>();


	XMLElement* animation_element = animator_element->FirstChildElement("animation");
	while (animation_element != nullptr) {
		HNodeInfo hnode_animation;

		const char* _name 		= animation_element->Attribute("name");
		const char* _fps 		= animation_element->Attribute("fps");
		const char* _loop_at 	= animation_element->Attribute("loopAt");
		if (_name == nullptr || _fps == nullptr) {
			std::cout << "Error: bad structure of animator" << std::endl;
			return nullptr;
		}

		hnode_animation.set_attribute("name", _name);
		hnode_animation.set_attribute("fps", _fps);

		std::string loop_at = _loop_at == nullptr ? "" : _loop_at;

		hnode_animation.set_attribute("loop_at", loop_at);

		XMLElement* instance_element = animation_element->FirstChildElement("instance");
		while (instance_element != nullptr) {
			HNodeInfo hnode_instance;

			const char* _name_instance 	= instance_element->Attribute("name");
			const char* _x_instance 	= instance_element->Attribute("x");
			const char* _y_instance 	= instance_element->Attribute("y");

			if (_name_instance == nullptr) {
				std::cout << "Error: bad structure of animator" << std::endl;
				return nullptr;
			}

			hnode_instance.set_attribute("name", _name_instance);

			std::string x_instance = _x_instance == nullptr ? "0" : _x_instance;
			std::string y_instance = _y_instance == nullptr ? "0" : _y_instance;

			hnode_instance.set_attribute("x", x_instance);
			hnode_instance.set_attribute("y", y_instance);

			hnode_animation.add_hnodeinfo("instance", hnode_instance);
			instance_element = instance_element->NextSiblingElement("instance");
		}

		hnode_root->add_hnodeinfo("animation", hnode_animation);
		animation_element = animation_element->NextSiblingElement("animation");
	}

	return hnode_root;
}

std::shared_ptr<HNodeInfo> HResources::get_hnodeinfo_map(const std::string& map_path) {
	if (map_hnodeinfo_map[ map_path ] != nullptr)
		return map_hnodeinfo_map[ map_path ];

	XMLDocument xml;
	XMLError eResult = xml.LoadFile(map_path.c_str());

	if (eResult != XML_SUCCESS) {
		std::cout << "Error: file " << map_path << " not found" << std::endl;
		return nullptr;
	}
	std::shared_ptr<HNodeInfo> hnode_root = std::make_shared<HNodeInfo>();
	XMLElement* root = xml.FirstChildElement("map");
	const char* map_width 		= root->Attribute("width");
	const char* map_height 		= root->Attribute("height");
	const char* map_tilewidth 	= root->Attribute("tilewidth");
	const char* map_tileheight 	= root->Attribute("tileheight");


	hnode_root->set_attribute("width", map_width);
	hnode_root->set_attribute("height", map_height);
	hnode_root->set_attribute("tilewidth", map_tilewidth);
	hnode_root->set_attribute("tileheight", map_tileheight);

	XMLElement* tileset_s = root->FirstChildElement("tileset");
	while (tileset_s != nullptr) {
		HNodeInfo hnode_tileset;

		const char* tileset_firstgid 	= tileset_s->Attribute("firstgid");
		const char* tileset_name 		= tileset_s->Attribute("name");
		const char* tileset_tilewidth 	= tileset_s->Attribute("tilewidth");
		const char* tileset_tileheight 	= tileset_s->Attribute("tileheight");
		const char* tileset_tilecount 	= tileset_s->Attribute("tilecount");
		const char* tileset_columns 	= tileset_s->Attribute("columns");

		XMLElement* tileset_image 			= tileset_s->FirstChildElement("image");
		const char* tileset_image_source 	= tileset_image->Attribute("source");
		const char* tileset_image_width 	= tileset_image->Attribute("width");
		const char* tileset_image_height 	= tileset_image->Attribute("height");

		hnode_tileset.set_attribute("firstgid", tileset_firstgid);
		hnode_tileset.set_attribute("name", tileset_name);
		hnode_tileset.set_attribute("tilewidth", tileset_tilewidth);
		hnode_tileset.set_attribute("tileheight", tileset_tileheight);
		hnode_tileset.set_attribute("tilecount", tileset_tilecount);
		hnode_tileset.set_attribute("columns", tileset_columns);

		hnode_tileset.set_attribute("image_source", tileset_image_source);
		hnode_tileset.set_attribute("image_width", tileset_image_width);
		hnode_tileset.set_attribute("image_height", tileset_image_height);

		// for properties of tiles
		XMLElement* tileset_tile = tileset_s->FirstChildElement("tile");
		while (tileset_tile != nullptr) {
			HNodeInfo hnode_tileset_tile;

			const char* tileset_tile_id	= tileset_tile->Attribute("id");
			hnode_tileset_tile.set_attribute("id", tileset_tile_id);

			XMLElement* tileset_tile_properties = tileset_tile->FirstChildElement("properties");
			XMLElement* tileset_tile_property = tileset_tile_properties->FirstChildElement("property");

			while (tileset_tile_property != nullptr) {
				HNodeInfo hnode_tileset_tile_property;

				const char* tileset_tile_property_name = tileset_tile_property->Attribute("name");
				const char* tileset_tile_property_type = tileset_tile_property->Attribute("type");
				tileset_tile_property_type = tileset_tile_property_type == nullptr ? "string" : tileset_tile_property_type;
				const char* tileset_tile_property_value = tileset_tile_property->Attribute("value");
				tileset_tile_property_value = tileset_tile_property_value == nullptr ? "" : tileset_tile_property_value;

				hnode_tileset_tile_property.set_attribute("name", tileset_tile_property_name);
				hnode_tileset_tile_property.set_attribute("type", tileset_tile_property_type);
				hnode_tileset_tile_property.set_attribute("value", tileset_tile_property_value);

				hnode_tileset_tile.add_hnodeinfo("property", hnode_tileset_tile_property);
				tileset_tile_property = tileset_tile_property->NextSiblingElement("property");
			}

			hnode_tileset.add_hnodeinfo("tile", hnode_tileset_tile);
			tileset_tile = tileset_tile->NextSiblingElement("tile");
		}

		hnode_root->add_hnodeinfo("tileset", hnode_tileset);
		tileset_s = tileset_s->NextSiblingElement("tileset");
	}

	XMLElement* layer_s = root->FirstChildElement("layer");
	while (layer_s != nullptr) {
		HNodeInfo hnode_layer;

		const char* layer_name = layer_s->Attribute("name");
		const char* layer_width = layer_s->Attribute("width");
		const char* layer_height = layer_s->Attribute("height");
		const char* layer_visible = layer_s->Attribute("visible");

		XMLElement* layer_data = layer_s->FirstChildElement("data");
		const char* layer_data_encoding = layer_data->Attribute("encoding");
		const char* layer_data_text = layer_data->GetText();

		hnode_layer.set_attribute("name", layer_name);
		hnode_layer.set_attribute("width", layer_width);
		hnode_layer.set_attribute("height", layer_height);
		if (layer_visible == nullptr)
			hnode_layer.set_attribute("visible", "");
		else
			hnode_layer.set_attribute("visible", layer_visible);

		hnode_layer.set_attribute("data_encoding", layer_data_encoding);
		hnode_layer.set_string_value(layer_data_text);

		hnode_root->add_hnodeinfo("layer", hnode_layer);
		layer_s = layer_s->NextSiblingElement("layer");
	}

	map_hnodeinfo_map[ map_path ] = hnode_root;

	return hnode_root;
}

std::shared_ptr<HNodeInfo> HResources::get_hnodeinfo_bitmap_font(const std::string& bitmap_font_path) {

	if (map_hnodeinfo_bitmap[ bitmap_font_path ] != nullptr)
		return map_hnodeinfo_bitmap[ bitmap_font_path ];

	XMLDocument xml;
	XMLError eResult = xml.LoadFile(bitmap_font_path.c_str());

	if (eResult != XML_SUCCESS) {
		std::cout << "Error: file " << bitmap_font_path << " not found" << std::endl;
		return nullptr;
	}

	std::shared_ptr<HNodeInfo> hnode_root = std::make_shared<HNodeInfo>();
	XMLElement* root = xml.FirstChildElement("font");

	XMLElement* common_s = root->FirstChildElement("common");
	
	const char* line_height = common_s->Attribute("lineHeight");
	const char* space_width = common_s->Attribute("space");
	const char* pages 		= common_s->Attribute("pages");

	hnode_root->set_attribute("line_height", line_height);
	hnode_root->set_attribute("space_width", space_width);
	hnode_root->set_attribute("pages", pages);

	HNodeInfo hnode_pages;
	XMLElement* pages_s	= root->FirstChildElement("pages");
	XMLElement* page_s 	= pages_s->FirstChildElement("page");
	while (page_s != nullptr) {
		HNodeInfo hnode_page;

		const char* page_id		= page_s->Attribute("id");
		const char* page_file	= page_s->Attribute("file");
	
		hnode_page.set_attribute("id", page_id);
		hnode_page.set_attribute("file", page_file);

		hnode_pages.add_hnodeinfo("page", hnode_page);
		page_s = page_s->NextSiblingElement("page");
	}
	hnode_root->add_hnodeinfo("pages", hnode_pages);

	HNodeInfo hnode_chars;
	XMLElement* chars_s	= root->FirstChildElement("chars");
	XMLElement* char_s	= chars_s->FirstChildElement("char");
	while (char_s != nullptr) {
		HNodeInfo hnode_char;

		const char* char_id	= char_s->Attribute("id");
		const char* char_x	= char_s->Attribute("x");
		const char* char_y	= char_s->Attribute("y");
		const char* char_width	= char_s->Attribute("width");
		const char* char_height	= char_s->Attribute("height");
		const char* char_xoffset	= char_s->Attribute("xoffset");
		const char* char_yoffset	= char_s->Attribute("yoffset");
		const char* char_xadvance	= char_s->Attribute("xadvance");
		const char* char_page	= char_s->Attribute("page");

		hnode_char.set_attribute("ascii", char_id);
		hnode_char.set_attribute("x", char_x);
		hnode_char.set_attribute("y", char_y);
		hnode_char.set_attribute("width", char_width);
		hnode_char.set_attribute("height", char_height);
		hnode_char.set_attribute("xoffset", char_xoffset);
		hnode_char.set_attribute("yoffset", char_yoffset);
		hnode_char.set_attribute("xadvance", char_xadvance);
		hnode_char.set_attribute("page", char_page);

		hnode_chars.add_hnodeinfo("char", hnode_char);
		char_s = char_s->NextSiblingElement("char");
	}
	hnode_root->add_hnodeinfo("chars", hnode_chars);

	HNodeInfo hnode_kernings;
	XMLElement* kernings_s	= root->FirstChildElement("kernings");
	XMLElement* kerning_s	= kernings_s->FirstChildElement("kerning");
	while (kerning_s != nullptr) {
		HNodeInfo hnode_kerning;

		const char* kerning_first = kerning_s->Attribute("first");
		const char* kerning_second = kerning_s->Attribute("second");
		const char* kerning_amount = kerning_s->Attribute("amount");

		hnode_kerning.set_attribute("first", kerning_first);
		hnode_kerning.set_attribute("second", kerning_second);
		hnode_kerning.set_attribute("amount", kerning_amount);

		hnode_kernings.add_hnodeinfo("kerning", hnode_kerning);
		kerning_s = kerning_s->NextSiblingElement("kerning");
	}

	hnode_root->add_hnodeinfo("kernings", hnode_kernings);
	
	map_hnodeinfo_bitmap[ bitmap_font_path ] = hnode_root;

	return hnode_root;
}

std::shared_ptr<HImageInstance> HResources::get_himageinstance(const std::string& name) {
	if (map_himageinstance[ name ] == nullptr) {
		std::cout << "Error: HResources doesn't found instance " << name << std::endl;
		return nullptr;
	 }


	return map_himageinstance[ name ];
}

void HResources::csv_to_vector(const std::string& csv, std::vector<int>& vec) {

	static std::string piece;
	int last = 0;
	for (int i = 0; i < csv.size(); i++) {
		if (csv[ i ] == ',') {

			piece = csv.substr(last, i - last);
			if (piece != "")
				vec.push_back(atoi(piece.c_str()));

			last = i + 1;
		}
	}

	piece = csv.substr(last);
	if (piece != "")
		vec.push_back(atoi(piece.c_str()));
}

void HResources::free() {
	map_htextures.clear();
	map_hsounds.clear();
	map_hnodeinfo_map.clear();
	map_himageinstance.clear();
}