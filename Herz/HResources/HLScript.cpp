#include "HLScript.h"

HLScript::HLScript() {

}

void HLScript::export_var(std::string name, std::string& v) {
	if (get_id_var(name) != -1) {
		std::cout << "Error: " << name << " var is already exported" << std::endl;
		return;
	}

	vector_var_name.push_back(name);
	vector_var_type.push_back("string");
	vector_var_ref.push_back(&v);
}
void HLScript::export_var(std::string name, int& v) {
	if (get_id_var(name) != -1) {
		std::cout << "Error: " << name << " var is already exported" << std::endl;
		return;
	}

	vector_var_name.push_back(name);
	vector_var_type.push_back("int");
	vector_var_ref.push_back(&v);
}
void HLScript::export_var(std::string name, double& v) {
	if (get_id_var(name) != -1) {
		std::cout << "Error: " << name << " var is already exported" << std::endl;
		return;
	}
	
	vector_var_name.push_back(name);
	vector_var_type.push_back("double");
	vector_var_ref.push_back(&v);
}
void HLScript::export_var(std::string name, ndec& v) {
	if (get_id_var(name) != -1) {
		std::cout << "Error: " << name << " var is already exported" << std::endl;
		return;
	}
	
	vector_var_name.push_back(name);
	vector_var_type.push_back("ndec");
	vector_var_ref.push_back(&v);
}

void HLScript::import_data(std::string path) {
	if ( ! is_file_exist(path.c_str())) {
		std::cout << "Error: import file data not found in " << path << std::endl;
		return;
	}

	static std::vector<std::string> tokens;
	tokens.clear();

	std::ifstream myReadFile;
	myReadFile.open(path);
	
	
	
	if ( ! myReadFile.is_open()) {
		std::cout << "Error: failed to open file" << std::endl;
		return;
	}
		
	std::string output;
	while (!myReadFile.eof()) {	
		
	    myReadFile >> output;

	    if (output == ";") {
	    	
    		int type_response = import_sentence(tokens);
    		
    		if (type_response != 0)
    			print_errors(type_response);
    	
	    	tokens.clear();
	    	continue;
	    }
	    
	    tokens.push_back(output);
	}

	myReadFile.close();
}

void HLScript::clear() {
	vector_var_name.clear();
	vector_var_type.clear();
	vector_var_ref.clear();
}

// private
std::vector<std::string> HLScript::split(const std::string &text, char sep) {
  static std::vector<std::string> tokens;
  tokens.clear();
  std::size_t start = 0, end = 0;
  while ((end = text.find(sep, start)) != std::string::npos) {
    tokens.push_back(text.substr(start, end - start));
    start = end + 1;
  }
  tokens.push_back(text.substr(start));
  return tokens;
}

bool HLScript::is_file_exist(const char *path) {
    std::ifstream infile(path);
    return infile.good();
}

void HLScript::print_errors(int e) {
	switch (e) {
		case 99:
			std::cout << "Error: sentence not recognized" << std::endl;
			break;
		case 1:
			std::cout << "Error: trying to assign a not valid int type" << std::endl;
			break;
		case 2:
			std::cout << "Error: trying to assign a not valid double type" << std::endl;
			break;
		case 3:
			std::cout << "Error: trying to assign a not valid ndec type" << std::endl;
			break;
	}
}

int HLScript::get_id_var(const std::string& var_name) {
	for (int i = 0; i < vector_var_name.size(); i++) {
		if (vector_var_name[ i ] == var_name)
			return i;
	}
	return -1;
}

bool HLScript::is_valid_int(const std::string& v){
	for (int i = 0; i < v.size(); i++)
		if (v[ i ] < '0' || v[ i ] > '9')
			return false;
	return true;
}

bool HLScript::is_valid_double(const std::string& v) {
	int n_points = 0;
	int n_nums = 0;
	int len = v.size();
	for (int i = 0; i < len; i++) {
		n_nums += v[ i ] >= '0' && v[ i ] <= '9';
		n_points += v[ i ] == '.';
	}
	if (n_nums == len)
		return true;
	if (n_nums == len - 1 && n_points == 1 && len >= 2 && v[ 0 ] != '.' && v[ len - 1 ] != '.')
		return true;

	return false;
}

int HLScript::import_sentence(const std::vector<std::string>& tokens) {

	bool is_assign = tokens[ 1 ] == "=";
	if (is_assign) {
		return assign_value(tokens);
	}
	

	return 99;
}

int HLScript::assign_value(const std::vector<std::string>& tokens) {
	int id = get_id_var(tokens[ 0 ]);
	if (id == -1)
		return 0;

	if (vector_var_type[ id ] == "string") {
		std::string total_string = "";
		for (int i = 2; i < tokens.size(); i++) {
			if (i == 2) {
				total_string += tokens[ i ];
			} else {
				total_string += " " + tokens[ i ];
			}
		}
		*(std::string*)vector_var_ref[ id ] = total_string;
		return 0;
	}

	if (vector_var_type[ id ] == "int") {
		if ( ! is_valid_int(tokens[ 2 ]))
			return 1;

		*(int*)vector_var_ref[ id ] = std::stoi(tokens[ 2 ]);
		return 0;
	}

	if (vector_var_type[ id ] == "double") {
		if ( ! is_valid_double(tokens[ 2 ]))
			return 2;

		*(double*)vector_var_ref[ id ] = std::stod(tokens[ 2 ]);
		return 0;
	}
	
	if (vector_var_type[ id ] == "ndec") {
		if ( ! is_valid_double(tokens[ 2 ]))
			return 3;

		*(ndec*)vector_var_ref[ id ] = ndec(std::stod(tokens[ 2 ]));
		return 0;
	}

	return 0;
}
