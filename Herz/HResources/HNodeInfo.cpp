#include "HNodeInfo.h"

HNodeInfo::HNodeInfo() {

}

const std::string& HNodeInfo::get_attribute(const std::string& id) const {

	try {
		const std::string& attribute = map_attributes.at( id );
		
		return attribute;
	
	} catch (const std::out_of_range& oor) {
		std::cout << "Error: hnodeinfo doesn't have " << id << " attribute" << std::endl;
		return nothing_string;
	}

}

const std::vector<HNodeInfo>& HNodeInfo::get_children(const std::string& id) const {
	
	int id_vector;
	try {
		id_vector = map_hnodes.at( id ) - 1;

  	} catch (const std::out_of_range& oor) {
  		std::cout << "Error: hnodeinfo doesn't have " << id << " children" << std::endl;
  		return nothing_vector;
  	}

	return vector_hnodes.at( id_vector );
}

const std::string& HNodeInfo::get_value() const {
	return string_value;
}

// private
void HNodeInfo::add_hnodeinfo(const std::string& id, const HNodeInfo& hnodeinfo) {
	int id_vector = map_hnodes[ id ] - 1;

	if (id_vector == -1) {

		vector_hnodes.push_back(std::vector<HNodeInfo>());
		id_vector = vector_hnodes.size();
		map_hnodes[ id ] = id_vector; 

		id_vector--;
	}

	vector_hnodes[ id_vector ].push_back(hnodeinfo);
}
void HNodeInfo::set_attribute(const std::string& id, const char* attr) {
	if (attr == nullptr)
		std::cout << "Error: HNodeInfo set_attribute trying to set a nullptr attribute " << id << std::endl;

	const std::string& attr_string = attr;
	set_attribute(id, attr_string);
}

void HNodeInfo::set_attribute(const std::string& id, const std::string& attr) {
	map_attributes[ id ] = attr;
}

void HNodeInfo::set_string_value(const char* value) {
	if (value == nullptr)
		std::cout << "Error: HNodeInfo set_string_value trying to set a nullptr value" << std::endl;

	const std::string& string_value = value;
	set_string_value(string_value);
}

void HNodeInfo::set_string_value(const std::string& string_value) {
	this->string_value = string_value;
}










