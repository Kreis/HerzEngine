#ifndef _HRESOURCES_H_
#define _HRESOURCES_H_

#include <memory>
#include <iostream>
#include <map>
#include <vector>
#include <HResources/HNodeInfo.h>
#include <HResources/HImageInstance.h>
#include "tinyxml/tinyxml2.h"
using namespace tinyxml2;

class HTexture;
class HSound;
class HResources {
public:
	HResources();

	void add_atlas(std::string path);

	std::shared_ptr<HTexture> get_htexture(const std::string& texture_path);
	std::shared_ptr<HSound> get_hsound(const std::string& sound_path);
	std::shared_ptr<HNodeInfo> get_hnodeinfo_animation(const std::string& animation_path);
	std::shared_ptr<HNodeInfo> get_hnodeinfo_map(const std::string& map_path);
	std::shared_ptr<HNodeInfo> get_hnodeinfo_bitmap_font(const std::string& bitmap_font_path);
	std::shared_ptr<HImageInstance> get_himageinstance(const std::string& name);
	void csv_to_vector(const std::string& csv, std::vector<int>& vec);
	void free();

private:
	std::map<std::string, std::shared_ptr<HTexture> > 	map_htextures;
	std::map<std::string, std::shared_ptr<HSound> > 	map_hsounds;
	std::map<std::string, std::shared_ptr<HNodeInfo> > 	map_hnodeinfo_map;
	std::map<std::string, std::shared_ptr<HNodeInfo> > map_hnodeinfo_bitmap;
	std::map<std::string, std::shared_ptr<HImageInstance> > map_himageinstance;
};

#endif