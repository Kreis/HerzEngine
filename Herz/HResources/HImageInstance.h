#ifndef _HIMAGEINSTANCE_H_
#define _HIMAGEINSTANCE_H_

#include <memory>
#include <iostream>
#include <HBasic/HintRect.h>

class HImageInstance {
public:
	HImageInstance(std::string path_texture, HintRect texture_rect);

	std::string path_texture;
	HintRect texture_rect;
};

#endif