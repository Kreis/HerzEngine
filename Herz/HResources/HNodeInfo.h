#ifndef _HNODEINFO_H_
#define _HNODEINFO_H_

#include <memory>
#include <iostream>
#include <map>
#include <vector>

class HNodeInfo {
	friend class HResources;
public:
	HNodeInfo();

	const std::string& get_attribute(const std::string& id) const;
	const std::vector<HNodeInfo>& get_children(const std::string& id) const;
	const std::string& get_value() const;

private:
	void add_hnodeinfo(const std::string& id, const HNodeInfo& hnodeinfo);
	void set_attribute(const std::string& id, const char* attr);
	void set_attribute(const std::string& id, const std::string& attr);
	void set_string_value(const char* string_value);
	void set_string_value(const std::string& string_value);

	std::map<std::string, std::string> map_attributes;
	std::map<std::string, int> map_hnodes;
	std::vector<std::vector<HNodeInfo> > vector_hnodes;
	std::string string_value;

	const std::vector<HNodeInfo> nothing_vector;
	const std::string nothing_string;
};

#endif