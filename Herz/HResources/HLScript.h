#ifndef _HLSCRIPT_H_
#define _HLSCRIPT_H_

#include <memory>
#include <iostream>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <HBasic/ndec.h>

class HLScript {
public:
	HLScript();

	void export_var(std::string name, std::string& v);
	void export_var(std::string name, int& v);
	void export_var(std::string name, double& v);
	void export_var(std::string name, ndec& v);

	void import_data(std::string path);

	void clear();

private:
	std::vector<std::string> vector_var_name;
	std::vector<std::string> vector_var_type;
	std::vector<void*> vector_var_ref;

	std::vector<std::string> split(const std::string& text, char sep);
	bool is_file_exist(const char *path);
	void print_errors(int e);
	bool is_valid_int(const std::string& v);
	bool is_valid_double(const std::string& v);

	int get_id_var(const std::string& var_name);
	int import_sentence(const std::vector<std::string>& tokens);
	int assign_value(const std::vector<std::string>& tokens);
};

#endif