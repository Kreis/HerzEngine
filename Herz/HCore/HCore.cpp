#include "HCore.h"

HCore::HCore() {}

HInputState* HCore::s_hinput() {
	static HInputState hinputstate;

	return &hinputstate;
}

HFactory* HCore::s_hfactory() {
	static HFactory hfactory(TypeFactory::SFML);

	return &hfactory;
}

HResources* HCore::s_hresources() {
	static HResources hresources;

	return &hresources;
}

HLScript* HCore::s_hlscript() {
	static HLScript hlscript;

	return &hlscript;
}

HPhysics* HCore::s_hphysics() {
	static HPhysics hphysics;

	return &hphysics;
}