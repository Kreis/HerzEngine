#ifndef _HCORE_H_
#define _HCORE_H_

#include <memory>
#include <iostream>
#include <map>
#include <vector>
#include <HStation/HInputState.h>
#include <HFactory/HFactory.h>
#include <HResources/HResources.h>
#include <HResources/HLScript.h>
#include <HPhysics/HPhysics.h>
#include <HBasic/HncRect.h>

class HCore {
public:
	static HInputState* s_hinput();
	static HFactory* s_hfactory();
	static HResources* s_hresources();
	static HLScript* s_hlscript();
	static HPhysics* s_hphysics();

private:
	HCore();
};

#endif