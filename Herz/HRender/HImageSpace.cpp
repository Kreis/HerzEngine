#include "HImageSpace.h"

HImageSpace::HImageSpace() {

}

void HImageSpace::reset() {
	vector_texture_priority.clear();
	vector_htexturedrawer.clear();
	hpool_texture_drawer.free_memory();
}

void HImageSpace::set_texture_priority(const std::vector<std::string>& vector_texture_priority) {
	this->vector_texture_priority = vector_texture_priority;

	vector_htexturedrawer.clear();
	hpool_texture_drawer.preload(50);
	for (int i = 0; i < vector_texture_priority.size(); i++) {
		vector_htexturedrawer.push_back(nullptr);
	}
}

void HImageSpace::draw(std::shared_ptr<HRender> hrender, bool clear_hrender, bool display_hrender) {

	if (clear_hrender)
		hrender->clear();
	
	for (std::size_t i = 0; i < vector_htexturedrawer.size(); i++) {

		if (vector_htexturedrawer[ i ] == nullptr)
			continue;

		HTextureDrawer* htexturedrawer = vector_htexturedrawer[ i ];

		htexturedrawer->draw(hrender);
		htexturedrawer->clear_texture();
		hpool_texture_drawer.push(htexturedrawer);
		vector_htexturedrawer[ i ] = nullptr;
	}

	if (display_hrender)
		hrender->display();
}

void HImageSpace::draw_himage(std::shared_ptr<HImage> himage) {
	std::string texture_name = himage->get_texture_name();

	int id = get_id_texturedrawer(texture_name);
	if (id == -1)
		return;


	if (vector_htexturedrawer[ id ] == nullptr) {
		vector_htexturedrawer[ id ] = hpool_texture_drawer.pop();
	}
	
	vector_htexturedrawer[ id ]->draw_himage(himage);
}

// private
int HImageSpace::get_id_texturedrawer(std::string name) {

	for (int i = 0; i < vector_texture_priority.size(); i++) {
		if (name == vector_texture_priority[ i ]) {
			return i;
		}
	}

	std::cout << "Warning: can't find priority texture to " << name << std::endl;
	return -1;
}


std::string HImageSpace::get_simple_texture_name(std::string name) const {
	std::size_t t = name.find("*");
	if (t != std::string::npos)
		return name.substr(0, t);
	return name;
}



