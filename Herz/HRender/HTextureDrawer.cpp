#include "HTextureDrawer.h"

HTextureDrawer::HTextureDrawer() {
	
}

void HTextureDrawer::draw_himage(std::shared_ptr<HImage> himage) {
	set_texture(himage->get_htexture());

	HintVector position = himage->get_position();
	const HintVector& size = himage->get_size();
	const HintRect& texture_coord = himage->get_texture_rect();
	HintVector texture_position = texture_coord.get_position();

	std::shared_ptr<HVertexArray> current_vertex = nullptr;

	if (himage->get_blend_type() == HBlend::BLEND_NONE) {
		current_vertex = vertex_none;
	}
	if (himage->get_blend_type() == HBlend::BLEND_ADD) {
		current_vertex = vertex_additive;
	}

	if (current_vertex == nullptr)
		return;

	for (std::shared_ptr<HVertex> hvertex : himage->get_vector_hvertex()) {
		current_vertex->push_vertex(hvertex);
	}
}
void HTextureDrawer::draw(std::shared_ptr<HRender> hrender) {
	if ( ! vertex_none->empty()) {
		hrender->draw(vertex_none, htexture, HBlend::BLEND_NONE);
	}
	vertex_none->clear();

	if ( ! vertex_additive->empty()) {
		hrender->draw(vertex_additive, htexture, HBlend::BLEND_ADD);
	}
	vertex_additive->clear();
}
void HTextureDrawer::clear_texture() {
	this->htexture = nullptr;
}

// private
void HTextureDrawer::set_texture(std::shared_ptr<HTexture> htexture) {
	if (this->htexture != nullptr)
		return;

	this->htexture = htexture;

	vertex_none = HCore::s_hfactory()->make_hvertexarray();
	vertex_additive = HCore::s_hfactory()->make_hvertexarray();
}













