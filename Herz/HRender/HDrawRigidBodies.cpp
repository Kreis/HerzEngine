#include "HDrawRigidBodies.h"

HDrawRigidBodies::HDrawRigidBodies() {
}

void HDrawRigidBodies::draw(
	std::shared_ptr<HImageSpace> himagespace,
	std::string path_htexture) {

	std::vector<std::shared_ptr<HRigidBody> >& vector_static_bodies = HCore::s_hphysics()->get_static_bodies();
	std::vector<std::shared_ptr<HRigidBody> >& vector_dynamic_bodies = HCore::s_hphysics()->get_dynamic_bodies();
	std::vector<std::shared_ptr<HSlope> >& vector_hslopes = HCore::s_hphysics()->get_hslopes();

	HintRect texture_coord(0, 0);
	for (std::shared_ptr<HRigidBody>& hrigidbody : vector_static_bodies) {

		texture_coord.size() = hrigidbody->size();

		std::shared_ptr<HImage> himage = std::make_shared<HImage>(path_htexture, texture_coord);
		himage->set_position(hrigidbody->get_position().geti());

		himagespace->draw_himage(himage);
	}

	for (std::shared_ptr<HRigidBody>& hrigidbody : vector_dynamic_bodies) {

		texture_coord.size() = hrigidbody->size();

		std::shared_ptr<HImage> himage = std::make_shared<HImage>(path_htexture, texture_coord);
		himage->set_position(hrigidbody->get_position().geti());

		himagespace->draw_himage(himage);
	}

	for (std::shared_ptr<HSlope>& hslope : vector_hslopes) {

		HintRect bound = hslope->get_bounds();
		texture_coord.size() = bound.size();

		std::shared_ptr<HImage> himage = std::make_shared<HImage>(path_htexture, texture_coord);
		himage->set_position(bound.get_position());

		himagespace->draw_himage(himage);
	}
}