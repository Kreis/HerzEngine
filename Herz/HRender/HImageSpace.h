#ifndef _HIMAGESPACE_H_
#define _HIMAGESPACE_H_

#include <memory>
#include <iostream>
#include <vector>
#include <map>
#include <HRender/HRender.h>
#include <HRender/HImage.h>
#include <HRender/HTextureDrawer.h>
#include <HBasic/HPoolObjects.h>
#include <stack>
#include <algorithm>

class HImageSpace {
public:
	HImageSpace();
	void reset();
	void set_texture_priority(const std::vector<std::string>& vector_texture_priority);
	void draw(std::shared_ptr<HRender> hrender, bool clear_hrender = true, bool display_hrender = true);
	void draw_himage(std::shared_ptr<HImage> himage);

private:
	std::vector<HTextureDrawer*> vector_htexturedrawer;
	std::vector<std::string> vector_texture_priority;
	HPoolObjects<HTextureDrawer> hpool_texture_drawer;

	int get_id_texturedrawer(std::string name);

	std::string get_simple_texture_name(std::string name) const;

	
};

#endif