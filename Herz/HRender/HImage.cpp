#include "HImage.h"

HImage::HImage(const std::string& texture_path) {

	this->htexture = HCore::s_hresources()->get_htexture(texture_path);
	
	int w;
	int h;
	htexture->get_size(w, h);
	const HintRect texture_rect(0, 0, w, h);

	init(texture_path, texture_rect);
}
HImage::HImage(const std::string& texture_path, const HintRect& texture_rect) {
	
	this->htexture = HCore::s_hresources()->get_htexture(texture_path);
	init(texture_path, texture_rect);
}
HImage::HImage(std::shared_ptr<HTexture>& htexture, std::string name) {

	this->htexture = htexture;

	int w;
	int h;
	htexture->get_size(w, h);
	const HintRect texture_rect(0, 0, w, h);

	init(name, texture_rect);
}
HImage::HImage(std::shared_ptr<HTexture>& htexture, std::string name, const HintRect& texture_rect) {

	this->htexture = htexture;
	init(name, texture_rect);
}

void HImage::set_position(const HintVector& position) {
	this->position = position;

	vector_hvertex[ 0 ]->set_position(position);
	vector_hvertex[ 1 ]->set_position(position.x + size.x, position.y);
	vector_hvertex[ 2 ]->set_position(position.x + size.x, position.y + size.y);
	vector_hvertex[ 3 ]->set_position(position.x, position.y + size.y);
}
void HImage::set_size(const HintVector& size) {
	this->size = size;

	vector_hvertex[ 1 ]->set_position(position.x + size.x, position.y);
	vector_hvertex[ 2 ]->set_position(position.x + size.x, position.y + size.y);
	vector_hvertex[ 3 ]->set_position(position.x, position.y + size.y);
}
void HImage::set_texture_rect(const HintRect& texture_rect) {
	this->texture_rect = texture_rect;

	const int& r_x = texture_rect.get_x();
	const int& r_y = texture_rect.get_y();
	const int& r_w = texture_rect.get_width();
	const int& r_h = texture_rect.get_height();
	vector_hvertex[ 0 ]->set_texture_coords(r_x, r_y);
	vector_hvertex[ 1 ]->set_texture_coords(r_x + r_w, r_y);
	vector_hvertex[ 2 ]->set_texture_coords(r_x + r_w, r_y + r_h);
	vector_hvertex[ 3 ]->set_texture_coords(r_x, r_y + r_h);
}
void HImage::set_blend_type(HBlend blend_type) {
	this->blend_type = blend_type;
}
void HImage::set_color(int r, int g, int b, int a) {
	color.clear();
	color.push_back(r);
	color.push_back(g);
	color.push_back(b);
	color.push_back(a);
}
const HintVector& HImage::get_position() const {
	return position;
}
const HintVector& HImage::get_size() const {
	return size;
}
const HintRect& HImage::get_texture_rect() const {
	return texture_rect;
}
std::shared_ptr<HTexture> HImage::get_htexture() const {
	return htexture;
}
const std::string& HImage::get_texture_name() const {
	return texture_path;
}
const HBlend& HImage::get_blend_type() const {
	return blend_type;
}
const std::vector<int>& HImage::get_color() const {
	return color;
}
void HImage::set_flip_horizontal(bool v) {
	if (flip_horizontal == v)
		return;
	flip_horizontal = v;

	update_flip();
}
void HImage::set_flip_vertical(bool v) {
	if (flip_vertical == v)
		return;
	flip_vertical = v;

	update_flip();
}
const std::vector<std::shared_ptr<HVertex> >& HImage::get_vector_hvertex() const {
	return vector_hvertex;
}
// private
void HImage::update_flip() {
	order_vertex.clear();
	if (flip_horizontal) {
		if (flip_vertical) {
			order_vertex.push_back(2);
			order_vertex.push_back(3);
			order_vertex.push_back(0);
			order_vertex.push_back(1);
		} else {
			order_vertex.push_back(1);
			order_vertex.push_back(0);
			order_vertex.push_back(3);
			order_vertex.push_back(2);
		}
	} else {
		if (flip_vertical) {
			order_vertex.push_back(3);
			order_vertex.push_back(2);
			order_vertex.push_back(1);
			order_vertex.push_back(0);
		} else {
			order_vertex.push_back(0);
			order_vertex.push_back(1);
			order_vertex.push_back(2);
			order_vertex.push_back(3);
		}
	}

	const int& r_x = texture_rect.get_x();
	const int& r_y = texture_rect.get_y();
	const int& r_w = texture_rect.get_width();
	const int& r_h = texture_rect.get_height();
	vector_hvertex[ order_vertex[0] ]->set_texture_coords(r_x, r_y);
	vector_hvertex[ order_vertex[1] ]->set_texture_coords(r_x + r_w, r_y);
	vector_hvertex[ order_vertex[2] ]->set_texture_coords(r_x + r_w, r_y + r_h);
	vector_hvertex[ order_vertex[3] ]->set_texture_coords(r_x, r_y + r_h);
	
}
void HImage::init(std::string name, const HintRect& texture_rect) {

	texture_path = name;

	const int& x = texture_rect.get_x();
	const int& y = texture_rect.get_y();
	const int& w = texture_rect.get_width();
	const int& h = texture_rect.get_height(); 

	vector_hvertex.push_back(HCore::s_hfactory()->make_hvertex(HintVector(0, 0), color, HintVector(x, y)));
	vector_hvertex.push_back(HCore::s_hfactory()->make_hvertex(HintVector(w, 0), color, HintVector(x + w, y)));
	vector_hvertex.push_back(HCore::s_hfactory()->make_hvertex(HintVector(w, h), color, HintVector(x + w, y + h)));
	vector_hvertex.push_back(HCore::s_hfactory()->make_hvertex(HintVector(0, h), color, HintVector(x, y + h)));

	set_size(HintVector(w, h));
	set_texture_rect(texture_rect);

	flip_horizontal = false;
	flip_vertical 	= false;
}



