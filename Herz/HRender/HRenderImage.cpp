#include "HRenderImage.h"

HRenderImage::HRenderImage(int width, int height) {
	himagespace = std::unique_ptr<HImageSpace>(new HImageSpace());
	hrendertexture = HCore::s_hfactory()->make_hrender_texture();

	hrendertexture->create(width, height);

	// himage = std::make_shared<HImage>();
}

void HRenderImage::create(int width, int height) {
	hrendertexture->create(width, height);
}
void HRenderImage::clear() {
	hrendertexture->clear();
}
void HRenderImage::draw(std::shared_ptr<HImage>& himage) {

}
void HRenderImage::display() {
	hrendertexture->display();
}

std::shared_ptr<HImage> HRenderImage::get_himage() {
	return himage;
}