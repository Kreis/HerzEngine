#ifndef _HRENDER_H_
#define _HRENDER_H_

#include <iostream>
#include <memory>
#include <HFactory/HBlend.h>

class HTexture;
class HVertexArray;
class HRender {
public:
	virtual void clear() = 0;
	virtual void draw(
		std::shared_ptr<HVertexArray>& vertexarray,
		std::shared_ptr<HTexture>& htexture,
		HBlend hblend = HBlend::BLEND_NONE) = 0;
	virtual void display() = 0;
};

#endif