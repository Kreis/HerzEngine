#ifndef _HIMAGE_H_
#define _HIMAGE_H_

#include <memory>
#include <iostream>
#include <HCore/HCore.h>
#include <HBasic/HintRect.h>
#include <HBasic/HintVector.h>

class HImage {
public:
	HImage(const std::string& texture_path);
	HImage(const std::string& texture_path, const HintRect& texture_rect);
	HImage(std::shared_ptr<HTexture>& htexture, std::string name);
	HImage(std::shared_ptr<HTexture>& htexture, std::string name, const HintRect& texture_rect);

	void set_position(const HintVector& position);
	void set_size(const HintVector& size);
	void set_texture_rect(const HintRect& texture_rect);
	void set_blend_type(HBlend blend_type);
	void set_color(int r, int g, int b, int a = 255);

	const HintVector& get_position() const;
	const HintVector& get_size() const;
	const HintRect& get_texture_rect() const;
	std::shared_ptr<HTexture> get_htexture() const;
	const std::string& get_texture_name() const;
	const HBlend& get_blend_type() const;
	const std::vector<int>& get_color() const;
	const std::vector<std::shared_ptr<HVertex> >& get_vector_hvertex() const;

	void set_flip_horizontal(bool v);
	void set_flip_vertical(bool v);

private:
	void init(std::string name, const HintRect& texture_rect);
	std::vector<int> order_vertex;
	void update_flip();
	std::vector<std::shared_ptr<HVertex> > vector_hvertex;
	std::shared_ptr<HTexture> htexture;

	std::string texture_path;
	HintRect texture_rect;
	HintVector position;
	HintVector size;
	HBlend blend_type = HBlend::BLEND_NONE;
	std::vector<int> color;

	bool flip_horizontal;
	bool flip_vertical;
};

#endif