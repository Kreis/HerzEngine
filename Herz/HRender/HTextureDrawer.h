#ifndef _HTEXTUREDRAWER_H_
#define _HTEXTUREDRAWER_H_

#include <memory>
#include <iostream>
#include <HFactory/HVertexArray.h>
#include <HRender/HImage.h>
#include <HRender/HRender.h>
#include <HBasic/HintVector.h>

class HTextureDrawer {
public:
	HTextureDrawer();
	void draw_himage(std::shared_ptr<HImage> himage);
	void draw(std::shared_ptr<HRender> hrender);
	void clear_texture();
private:
	void set_texture(std::shared_ptr<HTexture> htexture);

	std::shared_ptr<HTexture> htexture;
	std::shared_ptr<HVertexArray> vertex_none;
	std::shared_ptr<HVertexArray> vertex_additive;
};

#endif