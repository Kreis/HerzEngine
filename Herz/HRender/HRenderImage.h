#ifndef _HRENDERIMAGE_H_
#define _HRENDERIMAGE_H_

#include <iostream>
#include <memory>
#include <HRender/HImageSpace.h>
#include <HCore/HCore.h>

class HRenderImage {
public:
	HRenderImage(int width, int height);

	void create(int width, int height);
	void clear();
	void draw(std::shared_ptr<HImage>& himage);
	void display();

	std::shared_ptr<HImage> get_himage();

private:
	std::unique_ptr<HImageSpace> himagespace;
	std::shared_ptr<HRenderTexture> hrendertexture;
	std::shared_ptr<HImage> himage;
};

#endif