#ifndef _HDRAWRIGIDBODIES_H_
#define _HDRAWRIGIDBODIES_H_

#include <memory>
#include <iostream>
#include <HPhysics/HPhysics.h>
#include <HRender/HImageSpace.h>
#include <HCore/HCore.h>

class HDrawRigidBodies {
public:
	HDrawRigidBodies();

	void draw(
		std::shared_ptr<HImageSpace> himagespace,
		std::string path_htexture);


};

#endif