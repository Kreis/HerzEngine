#include "HFactory.h"

HFactory::HFactory(TypeFactory typefactory) {
	this->typefactory = typefactory;
}

std::shared_ptr<HRenderWindow> HFactory::make_hrender_window(
		int width, 
		int height, 
		std::string title, 
		int frame_rate, 
		bool full_screen
	) {
	switch (typefactory) {
		case TypeFactory::SFML:
			return std::shared_ptr<FRenderWindowSFML>(new FRenderWindowSFML(width, height, title, frame_rate, full_screen));
	}

	return nullptr;
}
std::shared_ptr<HTexture> HFactory::make_htexture(std::shared_ptr<HRenderTexture>& hrendertexture) {
	switch (typefactory) {
		case TypeFactory::SFML:
			return std::shared_ptr<FTextureSFML>(new FTextureSFML(hrendertexture));
	}

	return nullptr;
}
std::shared_ptr<HEvent> HFactory::make_hevent() {
	switch (typefactory) {
		case TypeFactory::SFML:
			return std::shared_ptr<FEventSFML>(new FEventSFML());
	}

	return nullptr;
}
std::shared_ptr<HCamera> HFactory::make_hcamera(HintRect area) {
	switch (typefactory) {
		case TypeFactory::SFML:
			return std::shared_ptr<FViewSFML>(new FViewSFML(area));
	}

	return nullptr;
}
std::shared_ptr<HVertexArray> HFactory::make_hvertexarray() {
	switch (typefactory) {
		case TypeFactory::SFML:
			return std::shared_ptr<FVertexArraySFML>(new FVertexArraySFML());
	}

	return nullptr;
}
std::shared_ptr<HTimer> HFactory::make_htimer() {
	switch (typefactory) {
		case TypeFactory::SFML:
			return std::shared_ptr<HTimer>(new FTimerSFML());
	}

	return nullptr;
}
std::shared_ptr<HDrawFPS> HFactory::make_hdrawfps(std::string font_path) {
	switch (typefactory) {
		case TypeFactory::SFML:
			return std::shared_ptr<HDrawFPS>(new FDrawFPSSFML(font_path));
	}

	return nullptr;
}
std::shared_ptr<HMusic> HFactory::make_hmusic() {
	switch (typefactory) {
		case TypeFactory::SFML:
			return std::shared_ptr<HMusic>(new FMusicSFML());
	}

	return nullptr;
}

std::shared_ptr<HVertex> HFactory::make_hvertex(HintVector position, const std::vector<int>& color, HintVector texture_coords) {
	switch (typefactory) {
		case TypeFactory::SFML:
			return std::shared_ptr<HVertex>(new FVertexSFML(position, color, texture_coords));
	}

	return nullptr;
}

std::shared_ptr<HRenderTexture> HFactory::make_hrender_texture() {
	switch (typefactory) {
		case TypeFactory::SFML:
			return std::shared_ptr<HRenderTexture>(new FRenderTextureSFML());
	}

	return nullptr;
}
std::shared_ptr<HThread> HFactory::make_hthread() {
	switch (typefactory) {
		case TypeFactory::SFML:
			return std::shared_ptr<HThread>(new FThreadSFML());
	}

	return nullptr;
}
std::shared_ptr<HMouse> HFactory::make_hmouse() {
	switch (typefactory) {
		case TypeFactory::SFML:
			return std::shared_ptr<HMouse>(new FMouseSFML());
	}

	return nullptr;
}
//private
std::shared_ptr<HTexture> HFactory::make_htexture() {
	switch (typefactory) {
		case TypeFactory::SFML:
			return std::shared_ptr<FTextureSFML>(new FTextureSFML());
	}

	return nullptr;
}
std::shared_ptr<HSound> HFactory::make_hsound() {
	switch (typefactory) {
		case TypeFactory::SFML:
			return std::shared_ptr<HSound>(new FSoundSFML());
	}

	return nullptr;
}
