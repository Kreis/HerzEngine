#ifndef _FDRAWFPSSFML_H_
#define _FDRAWFPSSFML_H_

#include <memory>
#include <iostream>
#include <HFactory/HDrawFPS.h>
#include <SFML/Graphics.hpp>
#include <HFactory/sfml/FRenderWindowSFML.h>

class FDrawFPSSFML : public HDrawFPS {
	friend class HFactory;
public:
	void draw_fps(std::shared_ptr<HRenderWindow> hrenderwindow, const ndec& delta_time, int x, int y) override;

private:
	FDrawFPSSFML(std::string font_path);
	sf::Text text;
	sf::Font* font;
	bool font_loaded;

	int calculated_fps;
	ndec fps_nc;
	int fps_count;
};

#endif