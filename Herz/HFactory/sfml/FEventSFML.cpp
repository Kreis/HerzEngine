#include "FEventSFML.h"

FEventSFML::FEventSFML() {
	ev = std::unique_ptr<sf::Event>(new sf::Event());
}

TypeEvent FEventSFML::get_type() {
	switch (ev->type) {
		case sf::Event::Closed:
			return TypeEvent::CLOSED;
		case sf::Event::KeyPressed:
			return TypeEvent::KEY_PRESSED;
		case sf::Event::KeyReleased:
			return TypeEvent::KEY_RELEASED;
		case sf::Event::Resized:
			return TypeEvent::RESIZED;
		case sf::Event::LostFocus:
			return TypeEvent::LOST_FOCUS;
		case sf::Event::GainedFocus:
			return TypeEvent::GAINED_FOCUS;
		case sf::Event::MouseWheelMoved:// deprecated but necessary
			return TypeEvent::MOUSE_WHEEL_MOVED;
		case sf::Event::MouseMoved:
			return TypeEvent::MOUSE_MOVED;
		case sf::Event::MouseEntered:
			return TypeEvent::MOUSE_ENTERED;
		case sf::Event::MouseLeft:
			return TypeEvent::MOUSE_LEFT;
		default:
			return TypeEvent::EMPTY;
	}
}

HKey FEventSFML::get_key_code() {
	switch (ev->key.code) {
		case sf::Keyboard::Up:
			return HKey::UP;
		case sf::Keyboard::Down:
			return HKey::DOWN;
		case sf::Keyboard::Left:
			return HKey::LEFT;
		case sf::Keyboard::Right:
			return HKey::RIGHT;
		case sf::Keyboard::A:
			return HKey::A;
		case sf::Keyboard::S:
			return HKey::S;
		default:
			return HKey::EMPTY;
	}
}

sf::Event* FEventSFML::get_sfml_value() {
	return ev.get();
}

bool FEventSFML::FEventSFML::is_shift() {
	return ev->key.shift;
}

bool FEventSFML::is_alt() {
	return ev->key.alt;
}

bool FEventSFML::is_control() {
	return ev->key.control;
}

void FEventSFML::get_mouse_wheel_delta(int& x, int& y) {
	y += ev->mouseWheel.delta;
}
