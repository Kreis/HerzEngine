#include "FDrawFPSSFML.h"


FDrawFPSSFML::FDrawFPSSFML(std::string font_path) : HDrawFPS(font_path) {
	font_loaded = false;
	font = new sf::Font();
	if ( ! font->loadFromFile(font_path))
		return;
	font_loaded = true;
	text.setFont(*font);
    text.setCharacterSize(16);
    text.setStyle(sf::Text::Bold);

	text.setFillColor(sf::Color::Red);
}

void FDrawFPSSFML::draw_fps(std::shared_ptr<HRenderWindow> hrenderwindow, const ndec& delta_time, int x, int y) {
	if ( ! font_loaded)
		return;

	FRenderWindowSFML* frenderwindowsfml = (FRenderWindowSFML*)hrenderwindow.get();
	sf::RenderWindow* renderwindowsfml = frenderwindowsfml->get_sfml_value();

	fps_nc += delta_time;
	fps_count++;
	if (fps_nc >= 1) {
		calculated_fps = fps_count;

		fps_count = 0;
		fps_nc = 0;
	}
	

	text.setPosition(x, y);
	std::string str = std::to_string(calculated_fps);
	text.setString(str);
	
	renderwindowsfml->draw(text);
}