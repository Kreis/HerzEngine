#ifndef _FMOUSESFML_H_
#define _FMOUSESFML_H_

#include <iostream>
#include <memory>
#include <vector>
#include <HFactory/HMouse.h>
#include <HFactory/sfml/FRenderWindowSFML.h>
#include <SFML/Graphics.hpp>

class FMouseSFML : public HMouse {
	friend class HFactory;
public:
	void set_position(HintVector& position,
		std::shared_ptr<HRenderWindow> hrender_window = nullptr) override;
	void set_position(int x, int y,
		std::shared_ptr<HRenderWindow> hrender_window = nullptr) override;
	HintVector get_position(std::shared_ptr<HRenderWindow> hrender_window = nullptr) override;
	bool is_typed(HMouseButton button) override;
	bool is_released(HMouseButton button) override;
	bool is_pressed(HMouseButton button) override;
	
	void update_mouse_state() override;
private:
	FMouseSFML();
	inline void update_single_buffer_value(bool v, int id) {
		// 0 not pressed, 1 typed, 2 pressed, 3 released
		if (v && (buffer[ id ] == 0 || buffer[ id ] == 1))
			buffer[ id ]++;
		else if ( ! v && (buffer[ id ] == 1 || buffer[ id ] == 2))
			buffer[ id ] = 3;
		else if ( ! v && buffer[ id ] == 3)
			buffer[ id ] = 0;
	}
	std::vector<int> buffer;
};

#endif