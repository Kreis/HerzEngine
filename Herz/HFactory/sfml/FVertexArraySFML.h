#ifndef _FVERTEXARRAYSFML_H_
#define _FVERTEXARRAYSFML_H_

#include <memory>
#include <iostream>
#include <HFactory/HVertexArray.h>
#include <HBasic/HintVector.h>
#include <SFML/Graphics.hpp>
#include <HFactory/sfml/FVertexSFML.h>

class FVertexArraySFML : public HVertexArray {
	friend class HFactory;
public:
	void push_vertex(std::shared_ptr<HVertex>& hvertex) override;
	void clear() override;
	bool empty() override;
	int size() override;

	const std::vector<sf::Vertex>& get_sfml_value();

private:
	FVertexArraySFML();

	std::vector<sf::Vertex> sfmlvertexarray;
};

#endif