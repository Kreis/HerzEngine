#ifndef _FEVENTSFML_H_
#define _FEVENTSFML_H_

#include <memory>
#include <iostream>
#include <SFML/Graphics.hpp>
#include <HBasic/HKey.h>
#include <HFactory/HEvent.h>

class FEventSFML : public HEvent {
	friend class HFactory;
public:
	TypeEvent get_type() override;
	HKey get_key_code() override;
	bool is_shift() override;
	bool is_alt() override;
	bool is_control() override;
	void get_mouse_wheel_delta(int& x, int& y) override;

	sf::Event* get_sfml_value();
private:
	FEventSFML();
	std::unique_ptr<sf::Event> ev;
};

#endif