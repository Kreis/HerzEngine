#include "FSoundSFML.h"

FSoundSFML::FSoundSFML() {
	sound = std::unique_ptr<sf::Sound>(new sf::Sound());
}

void FSoundSFML::load_from_file(std::string path) {
	if ( ! soundbuffer.loadFromFile(path)) {
		std::cout << "Warning: not found music file " << path << std::endl;
		return;
	}
	sound->setBuffer(soundbuffer);
}
void FSoundSFML::play() {
	sound->play();
}
void FSoundSFML::pause() {
	sound->pause();
}
void FSoundSFML::stop() {
	sound->stop();
}
void FSoundSFML::set_loop(bool v) {
	sound->setLoop(v);
}
void FSoundSFML::set_pitch(const ndec& n) {
	sound->setPitch(n.get());
}
void FSoundSFML::set_volume(const ndec& n) {
	sound->setVolume(n.get());
}

sf::Sound* FSoundSFML::get_sfml_value() {
	return sound.get();
}