#include "FMusicSFML.h"

FMusicSFML::FMusicSFML() {
	music = std::unique_ptr<sf::Music>(new sf::Music());
}
bool FMusicSFML::open_from_file(std::string path_music) {
	return music->openFromFile(path_music);
}
void FMusicSFML::play() {
	music->play();
}
void FMusicSFML::pause() {
	music->pause();
}
void FMusicSFML::stop() {
	music->stop();
}

sf::Music* FMusicSFML::get_sfml_value() {
	return music.get();
}