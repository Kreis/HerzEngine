#include "FTimerSFML.h"

FTimerSFML::FTimerSFML() {
	
}

const ndec FTimerSFML::get_delta_time() {
    time = clock.restart();

    return time.asSeconds();
}
