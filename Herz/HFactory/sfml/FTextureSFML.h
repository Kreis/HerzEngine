#ifndef _FTEXTURESFML_H_
#define _FTEXTURESFML_H_

#include <iostream>
#include <memory>
#include <HFactory/HTexture.h>
#include <SFML/Graphics.hpp>

class HRenderTexture;
class FTextureSFML : public HTexture {
	friend class HFactory;
	friend class FRenderTextureSFML;
public:
	bool load_from_file(const std::string& path) override;
	void get_size(int& width, int& height) override;
	
	const sf::Texture* get_sfml_value();
	
private:
	sf::RenderTexture* rendertexture_sfml;
	std::unique_ptr<sf::Texture> texture;
	std::shared_ptr<HRenderTexture> hrendertexture;

	FTextureSFML();
	FTextureSFML(std::shared_ptr<HRenderTexture>& hrendertexture);
};


#endif