#include "FMouseSFML.h"

void FMouseSFML::set_position(HintVector& position,
	std::shared_ptr<HRenderWindow> hrender_window) {
	set_position(position.x, position.y, hrender_window);
}

void FMouseSFML::set_position(int x, int y,
	std::shared_ptr<HRenderWindow> hrender_window) {
	if (hrender_window == nullptr) {
		sf::Mouse::setPosition(sf::Vector2i(x, y));
		return;
	}

	FRenderWindowSFML* frender_window_sfml = (FRenderWindowSFML*)hrender_window.get();
	sf::RenderWindow* sfml_render_window = frender_window_sfml->get_sfml_value();

	sf::Mouse::setPosition(sf::Vector2i(x, y), *sfml_render_window);	
}

HintVector FMouseSFML::get_position(std::shared_ptr<HRenderWindow> hrender_window) {
	if (hrender_window == nullptr) {
		sf::Vector2i vec = sf::Mouse::getPosition();
		return HintVector(vec.x, vec.y);
	}
	
	FRenderWindowSFML* frender_window_sfml = (FRenderWindowSFML*)hrender_window.get();
	sf::RenderWindow* sfml_render_window = frender_window_sfml->get_sfml_value();
	
	sf::Vector2i vec = sf::Mouse::getPosition(*sfml_render_window);
	return HintVector(vec.x, vec.y);
}

bool FMouseSFML::is_typed(HMouseButton button) {
	if (button == HMouseButton::LEFT)
		return buffer[ 0 ] == 1;
	else if (button == HMouseButton::RIGHT)
		return buffer[ 1 ] == 1;
	else if (button == HMouseButton::MIDDLE)
		return buffer[ 2 ] == 1;
	else if (button == HMouseButton::X1)
		return buffer[ 3 ] == 1;
	else if (button == HMouseButton::X2)
		return buffer[ 4 ] == 1;

	return false;
}

bool FMouseSFML::is_released(HMouseButton button) {
	if (button == HMouseButton::LEFT)
		return buffer[ 0 ] == 3;
	else if (button == HMouseButton::RIGHT)
		return buffer[ 1 ] == 3;
	else if (button == HMouseButton::MIDDLE)
		return buffer[ 2 ] == 3;
	else if (button == HMouseButton::X1)
		return buffer[ 3 ] == 3;
	else if (button == HMouseButton::X2)
		return buffer[ 4 ] == 3;

	return false;
}

bool FMouseSFML::is_pressed(HMouseButton button) {
	if (button == HMouseButton::LEFT)
		return buffer[ 0 ] == 1 || buffer[ 0 ] == 2;
	else if (button == HMouseButton::RIGHT)
		return buffer[ 1 ] == 1 || buffer[ 1 ] == 2;
	else if (button == HMouseButton::MIDDLE)
		return buffer[ 2 ] == 1 || buffer[ 2 ] == 2;
	else if (button == HMouseButton::X1)
		return buffer[ 3 ] == 1 || buffer[ 3 ] == 2;
	else if (button == HMouseButton::X2)
		return buffer[ 4 ] == 1 || buffer[ 4 ] == 2;

	return false;
}

void FMouseSFML::update_mouse_state() {
	update_single_buffer_value(sf::Mouse::isButtonPressed(sf::Mouse::Left), 0);
	update_single_buffer_value(sf::Mouse::isButtonPressed(sf::Mouse::Right), 1);
	update_single_buffer_value(sf::Mouse::isButtonPressed(sf::Mouse::Middle), 2);
	update_single_buffer_value(sf::Mouse::isButtonPressed(sf::Mouse::XButton1), 3);
	update_single_buffer_value(sf::Mouse::isButtonPressed(sf::Mouse::XButton2), 4);
}

//private
FMouseSFML::FMouseSFML() {
	int n = 5;
	while (n-->0)
		buffer.push_back(0);
}