#ifndef _FTIMERSFML_H_
#define _FTIMERSFML_H_

#include <memory>
#include <iostream>
#include <HBasic/ndec.h>
#include <HFactory/HTimer.h>
#include <SFML/Graphics.hpp>

class FTimerSFML : public HTimer {
	friend class HFactory;
public:
	const ndec get_delta_time() override;

private:
	FTimerSFML();
	sf::Clock clock;
	sf::Time time;
};

#endif