#ifndef _FSOUNDSFML_H_
#define _FSOUNDSFML_H_

#include <memory>
#include <iostream>
#include <HFactory/HSound.h>
#include <SFML/Audio.hpp>

class FSoundSFML : public HSound {
friend class HFactory;
public:
	void load_from_file(std::string path) override;
	void play() override;
	void pause() override;
	void stop() override;
	void set_loop(bool v) override;
	void set_pitch(const ndec& n) override;
	void set_volume(const ndec& n) override;

	sf::Sound* get_sfml_value();

private:
	FSoundSFML();

	sf::SoundBuffer soundbuffer;
	std::unique_ptr<sf::Sound> sound;
};

#endif