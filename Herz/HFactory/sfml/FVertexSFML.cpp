#include "FVertexSFML.h"

FVertexSFML::FVertexSFML(HintVector position, const std::vector<int>& color, HintVector texture_coords)
 : HVertex(position, color, texture_coords) {

	sf::Vector2f sfml_position = sf::Vector2f(position.x, position.y);
	sf::Vector2f sfml_texture_coords = sf::Vector2f(texture_coords.x, texture_coords.y);
	
	sf::Color selected_color;

	if (color.empty()) {
		selected_color = sf::Color::White;
	} else {
		int r = color[ 0 ];
		int g = color[ 1 ];
		int b = color[ 2 ];
		int a = color[ 3 ];
		selected_color = sf::Color(r, g, b, a);
	}

	sfml_vertex = std::unique_ptr<sf::Vertex>(new sf::Vertex(sfml_position, selected_color, sfml_texture_coords));
}
void FVertexSFML::set_position(const HintVector& position) {
	sfml_vertex->position = sf::Vector2f(position.x, position.y);
}
void FVertexSFML::set_position(const int x, const int y) {
	sfml_vertex->position = sf::Vector2f(x, y);
}
void FVertexSFML::set_texture_coords(const HintVector& texture_coords) {
	sfml_vertex->texCoords = sf::Vector2f(texture_coords.x, texture_coords.y);
}
void FVertexSFML::set_texture_coords(const int x, const int y) {
	sfml_vertex->texCoords = sf::Vector2f(x, y);
}
void FVertexSFML::set_color(const std::vector<int>& color) {

	sf::Color selected_color;

	if (color.empty()) {
		selected_color = sf::Color::White;
	} else {
		int r = color[ 0 ];
		int g = color[ 1 ];
		int b = color[ 2 ];
		int a = color[ 3 ];
		selected_color = sf::Color(r, g, b, a);
	}
	
	sfml_vertex->color = selected_color;
}
sf::Vertex* FVertexSFML::get_sfml_value() {
	return sfml_vertex.get();
}