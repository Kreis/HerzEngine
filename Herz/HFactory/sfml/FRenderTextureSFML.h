#ifndef _FRENDERTEXTURE_H_
#define _FRENDERTEXTURE_H_

#include <iostream>
#include <memory>
#include <HFactory/HRenderTexture.h>
#include <HFactory/sfml/FVertexArraySFML.h>
#include <HFactory/sfml/FTextureSFML.h>
#include <SFML/Graphics.hpp>

class FRenderTextureSFML : public HRenderTexture {
	friend class HFactory;
public:
	void create(int width, int height) override;
	void clear() override;
	void draw(
		std::shared_ptr<HVertexArray>& vertexarray,
		std::shared_ptr<HTexture>& htexture,
		HBlend hblend = HBlend::BLEND_NONE) override;
	void display() override;

	// sfml
	sf::RenderTexture* get_sfml_value();

private:
	FRenderTextureSFML();
	std::unique_ptr<sf::RenderTexture> rendertexture;
	sf::RenderStates renderstatesfml;
	bool created;
};

#endif