#include "FRenderTextureSFML.h"

FRenderTextureSFML::FRenderTextureSFML() {
	rendertexture = std::unique_ptr<sf::RenderTexture>(new sf::RenderTexture());
	created = false;
}

void FRenderTextureSFML::create(int width, int height) {
	rendertexture->create(width, height);
	created = true;
}

void FRenderTextureSFML::clear() {
	if ( ! created) {
		std::cout << "Warning HRenderTexture not created" << std::endl;
		return;
	}
	rendertexture->clear();
}

void FRenderTextureSFML::draw(
		std::shared_ptr<HVertexArray>& vertexarray,
		std::shared_ptr<HTexture>& htexture,
		HBlend hblend) {
	if ( ! created) {
		std::cout << "Warning HRenderTexture not created" << std::endl;
		return;
	}

	FTextureSFML* ftexturesfml = (FTextureSFML*)htexture.get();
	FVertexArraySFML* fvertexarraysfml = (FVertexArraySFML*)vertexarray.get();

	const sf::Texture* texturesfml = ftexturesfml->get_sfml_value();
	const std::vector<sf::Vertex>& vertexarraysfml = fvertexarraysfml->get_sfml_value();

	renderstatesfml.texture = texturesfml;

	switch (hblend) {
		case HBlend::BLEND_NONE:
			renderstatesfml.blendMode = sf::BlendAlpha;
			break;
		case HBlend::BLEND_ADD:
			renderstatesfml.blendMode = sf::BlendAdd;
			break;
		case HBlend::BLEND_MULTIPLY:
			renderstatesfml.blendMode = sf::BlendMultiply;
			break;
	}

	rendertexture->draw(&vertexarraysfml[0], vertexarraysfml.size(), sf::Quads, renderstatesfml);
	fvertexarraysfml->clear();

}

void FRenderTextureSFML::display() {
	if ( ! created) {
		std::cout << "Warning HRenderTexture not created" << std::endl;
		return;
	}
	rendertexture->display();
}


sf::RenderTexture* FRenderTextureSFML::get_sfml_value() {
	return rendertexture.get();
}