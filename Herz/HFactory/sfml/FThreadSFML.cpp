#include "FThreadSFML.h"

void FThreadSFML::initialize(std::function<void()>& fun) {
	sfmlthread = new sf::Thread(fun);
}
void FThreadSFML::run() {
	sfmlthread->launch();
}