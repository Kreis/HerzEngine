#ifndef _FMUSICSFML_H_
#define _FMUSICSFML_H_

#include <memory>
#include <iostream>
#include <HFactory/HMusic.h>
#include <SFML/Audio.hpp>

class FMusicSFML : public HMusic {
friend class HFactory;
public:
	bool open_from_file(std::string path_music) override; // .ogg
	void play() override;
	void pause() override;
	void stop() override;

	sf::Music* get_sfml_value();

private:
	FMusicSFML();
	std::unique_ptr<sf::Music> music;
};

#endif