#include "FRenderWindowSFML.h"

FRenderWindowSFML::FRenderWindowSFML(int width, int height, std::string title, int frame_rate, bool fullscreen)
: HRenderWindow(width, height, title, frame_rate, fullscreen) {
	if (fullscreen) {
		renderwindow = std::unique_ptr<sf::RenderWindow>(
			new sf::RenderWindow(sf::VideoMode(width, height), title, sf::Style::Fullscreen));
	} else {
		renderwindow = std::unique_ptr<sf::RenderWindow>(
			new sf::RenderWindow(sf::VideoMode(width, height), title));
	}
	//renderwindow->setFramerateLimit(frame_rate);
	renderwindow->setVerticalSyncEnabled(true);
}

void FRenderWindowSFML::create(int width, int height, std::string title, int frame_rate, bool fullscreen) {
	renderwindow->close();
	if (fullscreen) {
		renderwindow->create(sf::VideoMode(width, height), title, sf::Style::Fullscreen);
	} else {
		renderwindow->create(sf::VideoMode(width, height), title, sf::Style::Resize|sf::Style::Close);
	}
	// renderwindow->setFramerateLimit(frame_rate);
	renderwindow->setVerticalSyncEnabled(true);
}

bool FRenderWindowSFML::is_open() {
	return renderwindow->isOpen();
}

bool FRenderWindowSFML::poll_event(std::shared_ptr<HEvent> hevent) {
	FEventSFML* feventsfml = (FEventSFML*)hevent.get();
	sf::Event* ev = feventsfml->get_sfml_value();
	return renderwindow->pollEvent(*ev);
}

void FRenderWindowSFML::close() {
	renderwindow->close();
}

void FRenderWindowSFML::clear() {
	renderwindow->clear(sf::Color::Black);
}

void FRenderWindowSFML::display() {
	renderwindow->display();
}

void FRenderWindowSFML::draw(
	std::shared_ptr<HVertexArray>& vertexarray,
	std::shared_ptr<HTexture>& htexture,
	HBlend hblend) {
	
	FTextureSFML* ftexturesfml = (FTextureSFML*)htexture.get();
	FVertexArraySFML* fvertexarraysfml = (FVertexArraySFML*)vertexarray.get();

	const sf::Texture* texturesfml = ftexturesfml->get_sfml_value();
	const std::vector<sf::Vertex>& vertexarraysfml = fvertexarraysfml->get_sfml_value();

	renderstatesfml.texture = texturesfml;

	switch (hblend) {
		case HBlend::BLEND_NONE:
			renderstatesfml.blendMode = sf::BlendAlpha;
			break;
		case HBlend::BLEND_ADD:
			renderstatesfml.blendMode = sf::BlendAdd;
			break;
		case HBlend::BLEND_MULTIPLY:
			renderstatesfml.blendMode = sf::BlendMultiply;
			break;
	}

	renderwindow->draw(&vertexarraysfml[0], vertexarraysfml.size(), sf::Quads, renderstatesfml);
	fvertexarraysfml->clear();
}

void FRenderWindowSFML::set_camera(std::shared_ptr<HCamera> hcamera) {
	FViewSFML* fviewsfml = (FViewSFML*)hcamera.get();
	sf::View* view = fviewsfml->get_sfml_value();
	renderwindow->setView(*view);
}

void FRenderWindowSFML::get_size_frame(int& width, int& height) {
    width = renderwindow->getSize().x;
    height = renderwindow->getSize().y;
}

void FRenderWindowSFML::get_size_screen(int& width, int& height) {
    width = sf::VideoMode::getDesktopMode().width;
    height = sf::VideoMode::getDesktopMode().height;
}

sf::RenderWindow* FRenderWindowSFML::get_sfml_value() {
	return renderwindow.get();
}

