#include "FTextureSFML.h"
#include <HFactory/sfml/FRenderTextureSFML.h>

FTextureSFML::FTextureSFML() : HTexture() {
	this->rendertexture_sfml = nullptr;
	this->hrendertexture = nullptr;
}
FTextureSFML::FTextureSFML(std::shared_ptr<HRenderTexture>& hrendertexture) : HTexture(hrendertexture) {
	this->hrendertexture = hrendertexture;
	rendertexture_sfml = nullptr;
	this->texture = nullptr;
}

bool FTextureSFML::load_from_file(const std::string& path) {
	texture = std::unique_ptr<sf::Texture>(new sf::Texture());
	hrendertexture = nullptr;
	return texture->loadFromFile(path);
}

void FTextureSFML::get_size(int& width, int& height) {

	if (texture != nullptr) {
		width = texture.get()->getSize().x;
		height = texture.get()->getSize().y;
		return;
	}

	if (rendertexture_sfml == nullptr) {
		get_sfml_value();
	}

	width = rendertexture_sfml->getSize().x;
	height = rendertexture_sfml->getSize().y;
}

const sf::Texture* FTextureSFML::get_sfml_value() {

	if (texture != nullptr) {
		return texture.get();
	}
	
	if (rendertexture_sfml != nullptr) {
		return &rendertexture_sfml->getTexture();
	}
	if (hrendertexture != nullptr) {

		FRenderTextureSFML* frendertexturesfml = (FRenderTextureSFML*)hrendertexture.get();
		rendertexture_sfml = frendertexturesfml->get_sfml_value();
		auto v = &rendertexture_sfml->getTexture();

		return v;
	}

	std::cout << "Warning FTextureSFML doesn't have a sfml texture value" << std::endl;
	return nullptr;
}


