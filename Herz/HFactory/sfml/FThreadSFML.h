#ifndef _FTHREADSFML_H_
#define _FTHREADSFML_H_
#include <HFactory/HThread.h>
#include <SFML/Graphics.hpp>

class FThreadSFML : public HThread {
public:
	void initialize(std::function<void()>& fun) override;
	void run() override;

private:
	sf::Thread* sfmlthread;
};

#endif