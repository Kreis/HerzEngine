#include "FVertexArraySFML.h"

FVertexArraySFML::FVertexArraySFML() {}

void FVertexArraySFML::push_vertex(std::shared_ptr<HVertex>& hvertex) {
	FVertexSFML* fvertexsfml = (FVertexSFML*)hvertex.get();
	sf::Vertex* sfmlvertex = fvertexsfml->get_sfml_value();
	sfmlvertexarray.push_back(*sfmlvertex);
}
void FVertexArraySFML::clear() {
	sfmlvertexarray.clear();
}
bool FVertexArraySFML::empty() {
	return sfmlvertexarray.size() == 0;
}
int FVertexArraySFML::size() {
	return sfmlvertexarray.size();
}
const std::vector<sf::Vertex>& FVertexArraySFML::get_sfml_value() {
	return sfmlvertexarray;
}