#ifndef _FVIEWSFML_H_
#define _FVIEWSFML_H_

#include <memory>
#include <iostream>
#include <HFactory/HCamera.h>
#include <HBasic/HintRect.h>
#include <HBasic/HncRect.h>
#include <SFML/Graphics.hpp>

class FViewSFML : public HCamera {
	friend class HFactory;
public:
	void set_view(const HintRect& area) override;
	void set_viewport(const HncRect& area) override;
	const HintRect& get_view_rect() override;

	HncVector get_position() override;
	void set_position(const HncVector& position) override;
	void set_center(const HncVector& position) override;
	void set_center_x(const ndec& x) override;
	void set_center_y(const ndec& y) override;
	void move(const HncVector& position) override;

	sf::View* get_sfml_value();
private:
	FViewSFML(const HintRect& area);
	std::unique_ptr<sf::View> view;
	HintRect view_rect;

	double half_width;
	double half_height;
};

#endif