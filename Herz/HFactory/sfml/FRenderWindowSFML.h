#ifndef _FRENDERWINDOWSFML_H_
#define _FRENDERWINDOWSFML_H_
#include <memory>
#include <iostream>
#include <HFactory/HRenderWindow.h>
#include <HFactory/sfml/FEventSFML.h>
#include <HFactory/sfml/FViewSFML.h>
#include <HFactory/sfml/FVertexArraySFML.h>
#include <HFactory/sfml/FTextureSFML.h>
#include <SFML/Graphics.hpp>

class FRenderWindowSFML : public HRenderWindow {
	friend class HFactory;
public:
	void create(int width, int height, std::string title, int frame_rate, bool fullscreen = false) override;
	bool is_open() override;
	bool poll_event(std::shared_ptr<HEvent> hevent) override;
	void close() override;
	void clear() override;
	void display() override;
	void draw(
		std::shared_ptr<HVertexArray>& vertexarray,
		std::shared_ptr<HTexture>& htexture,
		HBlend hblend = HBlend::BLEND_NONE) override;
	void set_camera(std::shared_ptr<HCamera> hcamera) override;

	void get_size_frame(int& width, int& height) override;
	void get_size_screen(int& width, int& height) override;

	sf::RenderWindow* get_sfml_value();
	
private:
	FRenderWindowSFML(int width, int height, std::string title, int frame_rate, bool fullscreen = false);
	std::unique_ptr<sf::RenderWindow> renderwindow;
	sf::RenderStates renderstatesfml;
};


#endif