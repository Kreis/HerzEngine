#ifndef _FVERTEXSFML_H_
#define _FVERTEXSFML_H_
#include <SFML/Graphics.hpp>
#include <HFactory/HVertex.h>
#include <memory>

class FVertexSFML : public HVertex {
friend class HFactory;
public:
	void set_position(const HintVector& position) override;
	void set_position(const int x, const int y) override;
	void set_texture_coords(const HintVector& texture_coords) override;
	void set_texture_coords(const int x, const int y) override;
	void set_color(const std::vector<int>& color) override;

	sf::Vertex* get_sfml_value();
private:
	FVertexSFML(HintVector position, const std::vector<int>& color, HintVector texture_coords);
	std::unique_ptr<sf::Vertex> sfml_vertex;
};

#endif
