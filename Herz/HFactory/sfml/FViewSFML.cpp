#include "FViewSFML.h"

FViewSFML::FViewSFML(const HintRect& area) : HCamera(area) {
	view = std::unique_ptr<sf::View>(new sf::View(sf::FloatRect(area.get_x(), area.get_y(), area.get_width(), area.get_height())));
	half_width = area.get_width() / 2;
	half_height = area.get_height() / 2;

	modified = false;
}
void FViewSFML::set_view(const HintRect& area) {
	view->reset(sf::FloatRect(area.get_x(), area.get_y(), area.get_width(), area.get_height()));
	modified = false;
}
void FViewSFML::set_viewport(const HncRect& area) {
	view->setViewport(sf::FloatRect(area.get_x().get(), area.get_y().get(), area.get_width().get(), area.get_height().get()));
	modified = false;
}
const HintRect& FViewSFML::get_view_rect() {
	view_rect.x() = view->getCenter().x - view->getSize().x / 2;
	view_rect.y() = view->getCenter().y - view->getSize().y / 2;
	view_rect.width() = view->getSize().x;
	view_rect.height() = view->getSize().y;

	return view_rect;
}

sf::View* FViewSFML::get_sfml_value() {
	return view.get();
}

HncVector FViewSFML::get_position() {
	const sf::Vector2f& center = view->getCenter();

	return HncVector(center.x - half_width, center.y - half_height);
}

void FViewSFML::set_position(const HncVector& position) {
	view->setCenter(position.x + half_width, position.y + half_height);
	modified = true;
}

void FViewSFML::set_center(const HncVector& position) {
	view->setCenter(position.x.get(), position.y.get());
	modified = true;
}

void FViewSFML::set_center_x(const ndec& x) {
	const sf::Vector2f& center = view->getCenter();
	view->setCenter(x.get(), center.y);
	modified = true;
}

void FViewSFML::set_center_y(const ndec& y) {
	const sf::Vector2f& center = view->getCenter();
	view->setCenter(center.x, y.get());
	modified = true;
}

void FViewSFML::move(const HncVector& position) {
	const sf::Vector2f& center = view->getCenter();
	view->setCenter(position.x + center.x,  position.y + center.y);
	modified = true;
}