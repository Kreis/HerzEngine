#ifndef _HFACTORY_H_
#define _HFACTORY_H_

#include <memory>
#include <iostream>
#include <HBasic/HintRect.h>
#include <HFactory/HTexture.h>
#include <HFactory/HRenderWindow.h>
#include <HFactory/HEvent.h>
#include <HFactory/HCamera.h>
#include <HFactory/HVertexArray.h>
#include <HFactory/HDrawFPS.h>
#include <HFactory/HTimer.h>
#include <HFactory/HSound.h>
#include <HFactory/HMusic.h>
#include <HFactory/HRenderTexture.h>
#include <HFactory/sfml/FEventSFML.h>
#include <HFactory/sfml/FTextureSFML.h>
#include <HFactory/sfml/FRenderWindowSFML.h>
#include <HFactory/sfml/FViewSFML.h>
#include <HFactory/sfml/FVertexArraySFML.h>
#include <HFactory/sfml/FDrawFPSSFML.h>
#include <HFactory/sfml/FTimerSFML.h>
#include <HFactory/sfml/FSoundSFML.h>
#include <HFactory/sfml/FMusicSFML.h>
#include <HFactory/sfml/FVertexSFML.h>
#include <HFactory/sfml/FRenderTextureSFML.h>
#include <HFactory/sfml/FThreadSFML.h>
#include <HFactory/sfml/FMouseSFML.h>

enum class TypeFactory { SFML };

class HFactory {
	friend class HResources;
public:
	HFactory(TypeFactory typefactory);

	std::shared_ptr<HRenderWindow> make_hrender_window(
		int width, 
		int height, 
		std::string title, 
		int frame_rate, 
		bool full_screen = false);

	std::shared_ptr<HEvent> make_hevent();
	std::shared_ptr<HCamera> make_hcamera(HintRect area);
	std::shared_ptr<HVertexArray> make_hvertexarray();
	std::shared_ptr<HTimer> make_htimer();
	std::shared_ptr<HDrawFPS> make_hdrawfps(std::string font_path);
	std::shared_ptr<HMusic> make_hmusic();
	std::shared_ptr<HVertex> make_hvertex(HintVector position, const std::vector<int>& color, HintVector texture_coords);
	std::shared_ptr<HRenderTexture> make_hrender_texture();
	std::shared_ptr<HTexture> make_htexture(std::shared_ptr<HRenderTexture>& hrendertexture);
	std::shared_ptr<HThread> make_hthread();
	std::shared_ptr<HMouse> make_hmouse();
private:
	TypeFactory typefactory;

	std::shared_ptr<HTexture> make_htexture();
	std::shared_ptr<HSound> make_hsound();
};


#endif