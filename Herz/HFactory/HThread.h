#ifndef _HTHREAD_H_
#define _HTHREAD_H_
#include <iostream>
#include <memory>
#include <functional>

class HThread {
public:
	virtual void initialize(std::function<void()>& fun) = 0;
	virtual void run() = 0;
};

#endif