#ifndef _HTEXTURE_H_
#define _HTEXTURE_H_

#include <iostream>
#include <HFactory/HRenderTexture.h>

class HTexture {
public:
	explicit HTexture() {};
	explicit HTexture(std::shared_ptr<HRenderTexture>& hrendertexture) {};
	virtual bool load_from_file(const std::string& path) = 0;
	virtual void get_size(int& width, int& height) = 0;
};


#endif