#ifndef _HVERTEX_H_
#define _HVERTEX_H_
#include <HBasic/HintVector.h>
#include <vector>

class HVertex {
public:
	HVertex(HintVector position, const std::vector<int>& color, HintVector texture_coords) {}
	virtual void set_position(const HintVector& position) = 0;
	virtual void set_position(const int x, const int y) = 0;
	virtual void set_texture_coords(const HintVector& texture_coords) = 0;
	virtual void set_texture_coords(const int x, const int y) = 0;
	virtual void set_color(const std::vector<int>& color) = 0;
};

#endif