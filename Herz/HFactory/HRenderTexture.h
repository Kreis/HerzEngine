#ifndef _HRENDERTEXTURE_H_
#define _HRENDERTEXTURE_H_

#include <iostream>
#include <memory>
#include <HRender/HRender.h>

class HRenderTexture : public HRender {
public:
	virtual void create(int width, int height) = 0;
	virtual void clear() = 0;
	virtual void draw(
		std::shared_ptr<HVertexArray>& vertexarray,
		std::shared_ptr<HTexture>& htexture,
		HBlend hblend = HBlend::BLEND_NONE) = 0;
	virtual void display() = 0;

};

#endif