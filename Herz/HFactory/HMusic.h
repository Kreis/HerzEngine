#ifndef _HMUSIC_H_
#define _HMUSIC_H_

#include <memory>
#include <iostream>

class HMusic {
public:
	virtual bool open_from_file(std::string path_music) = 0; // .ogg
	virtual void play() = 0;
	virtual void pause() = 0;
	virtual void stop() = 0;
};

#endif