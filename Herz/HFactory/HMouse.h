#ifndef _HMOUSE_H_
#define _HMOUSE_H_

#include <memory>
#include <iostream>
#include <HBasic/HintVector.h>
#include <HFactory/HRenderWindow.h>

enum class HMouseButton {
	LEFT,
	RIGHT,
	MIDDLE,
	X1,
	X2,
	VERTICAL_WHEEL,
	HORIZONTAL_WHEEL
};

class HMouse {
public:
	virtual void update_mouse_state() = 0;
	virtual void set_position(HintVector& position,
		std::shared_ptr<HRenderWindow> hrender_window = nullptr) = 0;
	virtual void set_position(int x, int y,
		std::shared_ptr<HRenderWindow> hrender_window = nullptr) = 0;
	virtual HintVector get_position(std::shared_ptr<HRenderWindow> hrender_window = nullptr) = 0;
	virtual bool is_typed(HMouseButton button) = 0;
	virtual bool is_released(HMouseButton button) = 0;
	virtual bool is_pressed(HMouseButton button) = 0;
};

#endif