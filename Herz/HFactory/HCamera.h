#ifndef _HCAMERA_H_
#define _HCAMERA_H_

#include <memory>
#include <iostream>
#include <functional>
#include <HBasic/HintRect.h>
#include <HBasic/HncRect.h>

class HCamera {
public:
	HCamera(const HintRect& area) {}

	virtual HncVector get_position() = 0;
	virtual void set_position(const HncVector& position) = 0;
	virtual void set_center_x(const ndec& x) = 0;
	virtual void set_center_y(const ndec& y) = 0;
	virtual void set_center(const HncVector& position) = 0;
	virtual void move(const HncVector& position) = 0;

	virtual void set_view(const HintRect& area) = 0;
	virtual void set_viewport(const HncRect& area) = 0;
	virtual const HintRect& get_view_rect() = 0;

	bool modified;
};

#endif