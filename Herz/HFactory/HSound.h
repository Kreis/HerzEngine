#ifndef _HSOUND_H_
#define _HSOUND_H_

#include <memory>
#include <iostream>
#include <HBasic/ndec.h>

class HSound {
public:
	virtual void load_from_file(std::string path) = 0;
	virtual void play() = 0;
	virtual void pause() = 0;
	virtual void stop() = 0;
	virtual void set_loop(bool v) = 0;
	virtual void set_pitch(const ndec& n) = 0;
	virtual void set_volume(const ndec& n) = 0;
};

#endif