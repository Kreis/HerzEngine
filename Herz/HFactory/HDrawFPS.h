#ifndef _HDRAWFPS_H_
#define _HDRAWFPS_H_

#include <memory>
#include <iostream>
#include <string>
#include <HFactory/HRenderWindow.h>
#include <HBasic/ndec.h>

class HDrawFPS {
public:
	HDrawFPS(std::string font_path) {}
	virtual void draw_fps(std::shared_ptr<HRenderWindow> hrenderwindow, const ndec& delta_time, int x, int y) = 0;
};

#endif