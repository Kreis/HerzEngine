#ifndef _HBLEND_H_
#define _HBLEND_H_

enum HBlend {
	BLEND_NONE = 0,
	BLEND_ADD = 1,
	BLEND_MULTIPLY = 2
};

#endif