#ifndef _HEVENT_H_
#define _HEVENT_H_

#include <memory>
#include <iostream>
#include <HBasic/HKey.h>

enum class TypeEvent {
	CLOSED,
	KEY_PRESSED,
	KEY_RELEASED,
	RESIZED,
	LOST_FOCUS,
	GAINED_FOCUS,
	EMPTY,
	MOUSE_WHEEL_MOVED,
	MOUSE_MOVED,
	MOUSE_ENTERED,
	MOUSE_LEFT
};

class HEvent {
public:
	virtual TypeEvent get_type() = 0;
	virtual HKey get_key_code() = 0;
	virtual bool is_shift() = 0;
	virtual bool is_alt() = 0;
	virtual bool is_control() = 0;
	virtual void get_mouse_wheel_delta(int& x, int& y) = 0;
};

#endif