#ifndef _HTIMER_H_
#define _HTIMER_H_

#include <memory>
#include <iostream>
#include <HBasic/ndec.h>

class HTimer {
public:
	virtual const ndec get_delta_time() = 0;
};

#endif