#ifndef _HVERTEXARRAY_H_
#define _HVERTEXARRAY_H_

#include <memory>
#include <iostream>
#include <vector>
#include <HBasic/HintVector.h>
#include <HFactory/HVertex.h>

class HVertexArray {
public:
	virtual void push_vertex(std::shared_ptr<HVertex>& hvertex) = 0;
	virtual void clear() = 0;
	virtual bool empty() = 0;
	virtual int size() = 0;
};

#endif