#ifndef _HRENDERWINDOW_H_
#define _HRENDERWINDOW_H_

#include <memory>
#include <iostream>
#include <HFactory/HEvent.h>
#include <HFactory/HCamera.h>
#include <HFactory/HVertexArray.h>
#include <HFactory/HBlend.h>
#include <HFactory/HTexture.h>
#include <HRender/HRender.h>

class HRenderWindow : public HRender {
public:
	HRenderWindow(int width, int height, std::string title, int frame_rate, bool fullscreen = false) {}
	virtual void create(int width, int height, std::string title, int frame_rate, bool fullscreen = false) = 0;
	virtual bool is_open() = 0;
	virtual bool poll_event(std::shared_ptr<HEvent> hevent) = 0;
	virtual void close() = 0;
	virtual void clear() = 0;
	virtual void display() = 0;
	virtual void draw(
		std::shared_ptr<HVertexArray>& vertexarray,
		std::shared_ptr<HTexture>& htexture,
		HBlend hblend = HBlend::BLEND_NONE) = 0;
	virtual void set_camera(std::shared_ptr<HCamera> hcamera) = 0;

	virtual void get_size_frame(int& width, int& height) = 0;
	virtual void get_size_screen(int& width, int& height) = 0;
};


#endif