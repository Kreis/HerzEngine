#include <iostream>
#include <Herz.h>

#include "TestScene.h"

int main(int argc, char** argv) {
	
    std::shared_ptr<Herz> herz = std::make_shared<Herz>(400, 300, "Herz");


    std::shared_ptr<HScene> ts = std::make_shared<TestScene>();

    herz->start(ts);

 
	return 0;
}