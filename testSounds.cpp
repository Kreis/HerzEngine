#include <memory>
#include <iostream>
#include <Herz.h>

int main(int argc, char** argv) {


	std::shared_ptr<HWindow> hwindow = std::make_shared<HWindow>(100, 50, "test Sounds");


	// std::shared_ptr<HTimer> htimer = HCore::s_hfactory()->make_htimer();
	
	// sound
	
	std::shared_ptr<HSound> sound = HCore::s_hresources()->get_hsound("sound/battle.wav");

	// music
	/*
	sf::Music music;
	if ( ! music.openFromFile("sound/ff7.ogg")) {
		std::cout << "no cargo" << std::endl;
	}

*/
	std::shared_ptr<HMusic> music = HCore::s_hfactory()->make_hmusic();
	music->open_from_file("sound/ff7.ogg");
	
	while (hwindow->is_open()) {

		// const ndec t = htimer->get_delta_time();
		hwindow->update_input();

		hwindow->clear();

		if (HCore::s_hinput()->is_typed(HKey::UP)) {
			sound->play();
		}

		if (HCore::s_hinput()->is_typed(HKey::DOWN)) {
			//music.play();
			music->play();
		}

		if (HCore::s_hinput()->is_typed(HKey::LEFT)) {
			
		}
		if (HCore::s_hinput()->is_typed(HKey::RIGHT)) {
			
		}
		

		hwindow->display();

	}

	return 0;
}