#include <memory>
#include <iostream>
#include <Herz.h>

int main(int argc, char** argv) {


	std::shared_ptr<HWindow> hwindow = std::make_shared<HWindow>(600, 500, "test Animations");

	std::shared_ptr<HImageSpace> himagespace = std::make_shared<HImageSpace>();

	std::vector<std::string> tex_priority;
	tex_priority.push_back("texture/img2.png");
	himagespace->set_texture_priority(tex_priority);

	std::shared_ptr<HAnimation> hanimation = std::make_shared<HAnimation>();
	hanimation->set_texture("texture/img2.png");
	hanimation->add_frame(HintRect(0,0,40,40), 1);
	hanimation->add_frame(HintRect(40,0,40,40), 1);
	hanimation->add_frame(HintRect(80,0,40,40), 1);
	hanimation->add_frame(HintRect(120,0,40,40), 1);


	std::shared_ptr<HTimer> htimer = HCore::s_hfactory()->make_htimer();

	HAnimator hanimator;
	hanimator.add_hanimation("unica", hanimation);
	while (hwindow->is_open()) {

		const ndec t = htimer->get_delta_time();
		hwindow->update_input();

		hwindow->clear();

		if (HCore::s_hinput()->is_typed(HKey::UP)) {

		}

		hanimator.update(t);

		hanimator.draw(himagespace, HncVector(100, 100));

		hwindow->draw(himagespace);
		hwindow->display();

	}

	return 0;
}