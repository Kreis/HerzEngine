#ifndef _TESTSCENE2_H_
#define _TESTSCENE2_H_

#include <memory>
#include <iostream>
#include <Herz.h>

class TestScene2 : public HScene {
public:
	TestScene2();

	void load() override;
	void update (const ndec& delta_time, std::shared_ptr<HImageSpace> himagespace) override;

private:
	ndec current_time;
};

#endif